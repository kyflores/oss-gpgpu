#ifndef __SW_CPU_H
#define __SW_CPU_H

#include "sw_util.h"
#include "omp.h"
#include <string>
#include <cstdint>
#include <algorithm>


namespace swcpu{
  int16_t gen_scores(
    const std::string d,
    const std::string q,
    const int16_t match,
    const int16_t mismatch,
    const int16_t gap,
    std::vector< std::vector<int16_t> >& H,
    std::vector< std::vector<int16_t> >& T
  );


  int16_t traceback(
    const std::string& d,
    const std::string& q,
    std::vector< std::vector<int16_t> >& H,
    std::vector< std::vector<int16_t> >& T,
    std::string& d_al,
    std::string& q_al
  );

}

#endif // __SW_CPU_H
