#ifndef __SW_GPU_H
#define __SW_GPU_H

#include "sw_util.h"
#include <string>
// #include <utility>
#include <hcc/hc.hpp>
#include <cassert>
#include <vector>
#include <array>
#include <utility>
#include <iostream>
#include <hcc/hc.hpp>

#define CU 36*8
#define WAVEFRONT_SIZE 64

namespace swgpu{

  inline int __max(int a, int b) [[hc]];

  // 36 reflects Polaris 10's core configuration
  // Takes 36 strings, pads them up to the same size,
  // and finds the maximum H value for each grid.
  std::pair<hc::array<int, 1>, hc::array<int, 2> >prep_arrays(
    const std::string& d,
    const std::array<std::string, CU>& qs
  );

  std::pair<hc::array<short, 1>, hc::array<short, 2> >prep_arrays_short(
    const std::string& d,
    const std::array<std::string, CU>& qs
  );

  std::pair<hc::array<int, 1>, hc::array<int, 2> >prep_arrays_tri(
    const std::string& d,
    const std::array<std::string, CU>& qs
  );

  void gen_scores_many(
    const std::string& d,
    const std::array<std::string, CU>& qs, //
    const int match,
    const int mismatch,
    const int gap,
    std::array<int, CU>& result
  );
  void gen_scores_many_r1(
    const std::string& d,
    const std::array<std::string, CU>& qs, //
    const int match,
    const int mismatch,
    const int gap,
    std::array<int, CU>& result
  );

  void triangle_kernel(
    const std::string& d,
    const std::array<std::string, CU>& qs,
    const int match,
    const int mismatch,
    const int gap,
    std::array<int, CU>& result
  );



}

#endif // __SW_GPU_H
