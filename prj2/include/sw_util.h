#ifndef __SW_UTIL_H
#define __SW_UTIL_H

#include <vector>
#include <iostream>
#include <algorithm>
#include <cassert>
#include <cstdint>
#include <array>
#include <string>
#include <cstdlib>


namespace util{
  std::vector<short> seq_to_vec(const std::string& seq);
  std::vector<int> seq_to_vec_int(const std::string& seq);
  void reorder_seq(std::vector<int16_t>& vec, size_t lanes, bool cmd=false);
  void reorder_seq(std::vector<int>& vec, size_t lanes, bool cmd=false);
  int argmax(std::array<int,4>& arr);
  std::string random_sequence(size_t len, int seed);
}


#endif // __SW_UTIL_H
