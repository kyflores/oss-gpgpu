#ifndef __SW_PROTO_H
#define __SW_PROTO_H

#include "sw_util.h"
#include <string>
#include <cstdint>
// #include <utility>
#include <hcc/hc.hpp>
#include <cassert>
#include <vector>
#include <array>
#include <iostream>
#include <algorithm>
#include "omp.h"

//AVX has 256 bit width
#define SIMD_WIDTH 4
#define SIMD_WIDTH_32 8
#define SIMD_WIDTH_16 16

namespace swhc{

  void vec_test(std::vector<int32_t>& y, std::vector<int32_t> x, int32_t a, int32_t b);

  inline int16_t __max(int16_t a, int16_t b);

  void process_row(
    std::vector<int16_t>& i_seq, // integer encoding of query sequence
    std::vector<int16_t>& h_top, // incoming h from the row above.
    const int16_t q_value, // the column's nucleotide value in d,
    std::vector<int16_t>& H, // Destination for a column of H
    std::vector<int16_t>& T, // Destination for a column of T (backtrace key)
    const int16_t match,
    const int16_t mismatch,
    const int16_t gap
  );

  int16_t gen_scores(
    const std::string d,
    const std::string q,
    const int16_t match,
    const int16_t mismatch,
    const int16_t gap,
    std::vector< std::vector<int16_t> >& H,
    std::vector< std::vector<int16_t> >& T
  );

  int16_t gen_max_score(
    const std::string d,
    const std::string q,
    const int16_t match,
    const int16_t mismatch,
    const int16_t gap
  );

}

#endif // __SW_PROTO_H
