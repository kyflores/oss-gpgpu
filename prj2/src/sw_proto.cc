#include "sw_proto.h"

void swhc::vec_test(std::vector<int32_t>& y, std::vector<int32_t> x, int32_t a, int32_t b){
  #pragma omp simd
  for(size_t i = 1; i < x.size(); i++){
    y[i] = a*x[i]+b;
  }
}

inline int16_t swhc::__max(int16_t a, int16_t b){
  return (a > b) ? a : b;
}

void swhc::process_row(
  // Operates on a NONstriped sequence.
  std::vector<int16_t>& i_seq, // integer encoding of query sequence
  std::vector<int16_t>& h_top, // incoming h from the row above.
  const int16_t q_value, // the column's nucleotide value in d,
  std::vector<int16_t>& H, // Destination for a column of H
  std::vector<int16_t>& T, // Destination for a column of T (backtrace key)
  const int16_t match,
  const int16_t mismatch,
  const int16_t gap
){
  auto score = [=] (int16_t a, int16_t b) {return (a == b) ? match : mismatch;};
  assert(i_seq.size()+1 == h_top.size()); // h top must be width padded.

  // Input will be a row with s groups of N lanes.
  // [--1--][--2--]...[--s--]. each [-----] is N wide.
  size_t s = i_seq.size()/SIMD_WIDTH;
  for(size_t j = 0; j < s; j++){
    #pragma omp simd
    for(size_t i = 0; i < SIMD_WIDTH; i++){
      const size_t col = i * s + j + 1;
      std::array<int16_t, 4> cands = {0};
      cands[0] = h_top[col-1] + score(q_value, i_seq[col-1]);
      cands[1] = H[col-1] - gap;
      cands[2] = h_top[col] - gap;
      cands[3] = 0;

      int16_t m = 0;
      int16_t argm = 3;

      #pragma unroll
      for(int x = 0; x < 4; x++){
          argm = (cands[x]>m) ? x : argm;
          m = (cands[x]>m) ? cands[x] : m;
      }

      H[col] = m;
      T[col] = argm;
    }
  }
  // Correction loop.
  uint32_t loop_exec_count = 0;
  size_t j = 0;

  uint32_t cmp_mask = 0x1; // Bitmask, means that lane count must be 32 or less.

  while(cmp_mask != 0){
  // while(false){
    loop_exec_count++;

    // #pragma omp simd
    for(size_t i = 0; i < SIMD_WIDTH; i++){
      const size_t col = i * s + j + 1;
      if((H[col-1]-gap > H[col])){
        // if the West value is greater than the value we wrote when we
        // assumed the West value was 0, then the current H should be set
        // to the West value - gap penalty. The trace value is then 1 (W)
        H[col] = H[col-1]-gap;
        T[col] = 1;
        cmp_mask |= (1<<i);
      }
      else{
        cmp_mask &= ~(1<<i);
       }
     }
     j++;
     if(j>=s){j=0;}
  }
}

int16_t swhc::gen_scores(
  const std::string d,
  const std::string q,
  const int16_t match,
  const int16_t mismatch,
  const int16_t gap,
  std::vector< std::vector<int16_t> >& H,
  std::vector< std::vector<int16_t> >& T
){
  assert(d.length() % SIMD_WIDTH ==0);
  std::vector<int16_t> ds = util::seq_to_vec(d);
  std::vector<int16_t> qs = util::seq_to_vec(q);
  const size_t rows = qs.size()+1;
  const size_t cols = ds.size()+1;

  std::vector< std::vector<int16_t> > H_grid;
  H_grid.resize(rows);
  for(auto &v : H_grid){
    v.resize(cols);
    std::fill(v.begin(), v.end(), 0);
  }

  std::vector< std::vector<int16_t> > T_grid;
  T_grid.resize(rows);
  for(auto &v : T_grid){
    v.resize(cols);
    std::fill(v.begin(), v.end(), 0);
  }

  // Do the actual computation across all rows.
  for(size_t r = 1; r < rows; r++){
    process_row(ds, H_grid[r-1], qs[r-1], H_grid[r],
       T_grid[r], match, mismatch, gap);
  }
  int16_t reval = 0;
  for(auto &x : H_grid){
    for(auto y : x){
      reval = (y>reval) ? y : reval;
    }
  }

  H = H_grid;
  T = T_grid;

  return reval;
}

int16_t swhc::gen_max_score(
  const std::string d,
  const std::string q,
  const int16_t match,
  const int16_t mismatch,
  const int16_t gap
){
  assert(d.length() % SIMD_WIDTH == 0);
  std::vector<int16_t> ds = util::seq_to_vec(d);
  // for(auto i : ds){
  //   std::cout << i;
  // }
  // std::cout<<"\n";
  util::reorder_seq(ds, SIMD_WIDTH, false);
  // for(auto i : ds){
  //   std::cout << i;
  // }
  // std::cout<<"\n";


  std::vector<int16_t> qs = util::seq_to_vec(q);
  const size_t cols = ds.size()+1;
  const size_t rows = qs.size()+1;
  size_t s = ds.size()/SIMD_WIDTH;
  auto score = [=] (int16_t a, int16_t b) {return (a == b) ? match : mismatch;};

  // The entire row above. Where the Northern values come in from.
  std::vector<int16_t> H_enter(cols-1, 0);
  // The current working row. Where we store everything after a full col loop.
  std::vector<int16_t> H_exit(cols-1, 0);

  int16_t global_max = -1 ;
  for(size_t r = 1; r < rows; r++){
    // parallel section entering data.
    std::array<int16_t, SIMD_WIDTH> _h_ld;
    _h_ld.fill(0);
    for(size_t j = 0; j < s; j++){

      // Main parallel loop
      #pragma omp simd
      for(size_t i = 0; i < SIMD_WIDTH; i++){
        const size_t col = j * SIMD_WIDTH + i + 1; // Global column
        int16_t h_n = H_enter[col] - gap;
        int16_t h_w = _h_ld[i] - gap;

        // TODO: All this junk is to index the northwest value.
        int32_t j0_modifier = (SIMD_WIDTH-1)*s - 1;
        int32_t idx_modifier;
        idx_modifier = (j == 0) ? j0_modifier : (-SIMD_WIDTH);
        int16_t h_imm = (col == 1) ? 0 : H_enter[col+idx_modifier];
        int16_t h_nw = h_imm + score(ds[col-1] , qs[r-1]);

        // std::cout<< col << ", " << h_n << ", " << h_w << ", " << h_nw << "\n";
        int16_t m = __max(h_n, h_w);
        m = __max(h_nw, m);
        m = __max(0, m);

        _h_ld[i] = m;
        H_exit[col] = m;
      }
    }
    std::swap(H_enter,H_exit);
    // std::cout << "Ending parallel section with " <<std::endl;
    // for(size_t i = 0; i < H_enter.size(); i++){
    //   std::cout<< H_enter[i+1] << ", ";
    // }
    // std::cout<<std::endl;

    // Shift final exiting values left in _hst
    // std::cout<<"Ending h_ld with " << _h_ld[0] <<", "<<_h_ld[1]<<std::endl;
    for(size_t i = SIMD_WIDTH-1; i >= 1; i--){
      _h_ld[i] = _h_ld[i-1];
    }
    _h_ld[0] = 0;
    // std::cout<<"Starting correct with " << _h_ld[0] <<", "<<_h_ld[1]<<", "
      // <<_h_ld[2]<<", "<<_h_ld[3]<<std::endl;

    // This loop must enter true the first time.

    size_t j = 0;
    std::array<int16_t, SIMD_WIDTH> cmp;
    cmp.fill(true);
    uint32_t iters = 0;
    while(std::any_of(cmp.begin(),cmp.end(),[](bool x){return x;})){
      iters++;
      #pragma omp simd
      for(size_t i = 0; i < SIMD_WIDTH; i++){
        const size_t col = j * SIMD_WIDTH + i + 1;
        int16_t h = H_enter[col];
        int16_t h_w = _h_ld[i];
        // std::cout << "Col: " << col << " H: " << h << " HW: " << h_w << std::endl;
        cmp[i] = (h_w-gap > h);
        if(cmp[i]){
          // std::cout << "Value corrected in " <<col<< '\n';
          H_enter[col] = h_w-gap;
          _h_ld[i] = h_w-gap;
        }
      }
      j++;
      if(j>=s){
        j=0;
        for(size_t i = SIMD_WIDTH-1; i >= 1; i--){
          _h_ld[i] = _h_ld[i-1];
        }
      }
    }
    // std::cout << "Ending correct at " <<iters <<" iterations."<< '\n';
    for(size_t i = 0; i < H_enter.size(); i++){
      // std::cout<< H_enter[i+1] << ", ";
      global_max = __max(global_max, H_enter[i]);
    }
    // std::cout<<std::endl;

  }
  return global_max;
}
