#include "sw_gpu.h"

// Crazy pair thing b/c "-" doesn't seem to move the way I thought?
std::pair<hc::array<int, 1>, hc::array<int, 2> > swgpu::prep_arrays(
  const std::string& d,
  const std::array<std::string, CU>& qs
){
  std::vector<int> ds_h = util::seq_to_vec_int(d);
  assert(ds_h.size() % WAVEFRONT_SIZE == 0);

  size_t qs_len = qs[0].length();
  // hc::array constructs with a giant flat array and dims, so need to flatten
  std::vector<int> qs_flat = util::seq_to_vec_int(qs[0]);

  // Flatten qs arrays into a 1D vector for use w/ hc::array
  for(size_t i = 1; i < CU; i++){
    assert(qs[i].length() == qs_len); // Need to go pad qs beforehand with N
    std::vector<int> tmp = util::seq_to_vec_int(qs[i]);
    qs_flat.insert(qs_flat.end(), tmp.begin(), tmp.end());
  }

  hc::extent<1> d_ext(ds_h.size());
  hc::extent<2> qs_ext(CU, qs_len);

  hc::array<int, 1> d_hc(d_ext, ds_h.begin(), ds_h.end());
  hc::array<int, 2> qs_hc(qs_ext, qs_flat.begin(), qs_flat.end());

  return std::make_pair(d_hc, qs_hc);
}

std::pair<hc::array<short, 1>, hc::array<short, 2> > swgpu::prep_arrays_short(
  const std::string& d,
  const std::array<std::string, CU>& qs
){
  std::vector<short> ds_h = util::seq_to_vec(d);
  assert(ds_h.size() % WAVEFRONT_SIZE == 0);

  size_t qs_len = qs[0].length();
  // hc::array constructs with a giant flat array and dims, so need to flatten
  std::vector<short> qs_flat = util::seq_to_vec(qs[0]);

  // Flatten qs arrays into a 1D vector for use w/ hc::array
  for(size_t i = 1; i < CU; i++){
    assert(qs[i].length() == qs_len); // Need to go pad qs beforehand with N
    std::vector<short> tmp = util::seq_to_vec(qs[i]);
    qs_flat.insert(qs_flat.end(), tmp.begin(), tmp.end());
  }

  hc::extent<1> d_ext(ds_h.size());
  hc::extent<2> qs_ext(CU, qs_len);

  hc::array<short, 1> d_hc(d_ext, ds_h.begin(), ds_h.end());
  hc::array<short, 2> qs_hc(qs_ext, qs_flat.begin(), qs_flat.end());

  return std::make_pair(d_hc, qs_hc);
}

std::pair<hc::array<int, 1>, hc::array<int, 2> > swgpu::prep_arrays_tri(
  const std::string& d,
  const std::array<std::string, CU>& qs
){
  // Add the padding for off-grid cells.
  std::vector<int> ds_h = util::seq_to_vec_int(d);
  std::vector<int> pad(WAVEFRONT_SIZE-1,-1);
  ds_h.insert(ds_h.begin(), pad.begin(), pad.end());
  ds_h.insert(ds_h.end(), pad.begin(), pad.end());

  size_t qs_len = qs[0].length();
  // assert(qs_len % WAVEFRONT_SIZE == 0);
  std::vector<int> qs_flat = util::seq_to_vec_int(qs[0]);
  for(size_t i = 1; i < CU; i++){
    assert(qs[i].length() == qs_len); // Need to go pad qs beforehand with N
    std::vector<int> tmp = util::seq_to_vec_int(qs[i]);
    qs_flat.insert(qs_flat.end(), tmp.begin(), tmp.end());
  }

  hc::extent<1> d_ext(ds_h.size());
  hc::extent<2> qs_ext(CU, qs_len);

  hc::array<int, 1> d_hc(d_ext, ds_h.begin(), ds_h.end());
  hc::array<int, 2> qs_hc(qs_ext, qs_flat.begin(), qs_flat.end());

  return std::make_pair(d_hc, qs_hc);
}

inline int swgpu::__max(int a, int b) [[hc]]{
  return (a > b) ? a : b;
}

void swgpu::gen_scores_many(
  const std::string& d,
  const std::array<std::string, CU>& qs,
  const int match,
  const int mismatch,
  const int gap,
  std::array<int, CU>& result
){
  /* Since we are comparing 1 seq to many, strategy is to give each of "many"
   * to a different workgroup. The smallest unit that can branch separately is
   * an entire wavefront, so different sequences must not be analyzed in the
   * same wavefront. There can be more than 64 "lanes" by increasing the workgroup
   * size, but the penalty for guessing the left value wrong increases.
  */
  auto arrs = swgpu::prep_arrays(d, qs);
  hc::array<int, 1>& d_hc = std::get<0>(arrs);
  hc::array<int, 2>& q_hc = std::get<1>(arrs);

  // Sizes for grid computation, +1 is to account for the row of 0's.
  const unsigned rows = q_hc.get_extent()[1]+1;
  const unsigned cols = d_hc.get_extent()[0]+1;

  // Check data sizes...
  assert(rows == qs[0].length()+1);
  assert(cols == d.length()+1);

  // s is the number of cells each lane will process.
  unsigned s = (cols-1) / WAVEFRONT_SIZE;

  // Maximum values from each of 36 sequences goes in here.
  hc::array<int, 1> res_hc(CU);

  // Scratch arrays to use during computation.
  // Each workgroup needs a scratch array.
  std::vector<int> _h(CU * cols * 2, 0);
  hc::extent<3> sc_ext(CU, cols, 2);
  hc::array<int, 3> h0(sc_ext, _h.begin(), _h.end());

  // Tile to make each Workgroup contain a single wavfront.
  hc::extent<1> iext(CU * WAVEFRONT_SIZE);
  hc::tiled_extent<1> t_iext = iext.tile(WAVEFRONT_SIZE);

  auto score = [=] (int a, int b) [[hc]] {return (a == b) ? match : mismatch;};

  // std::cout<<"s: " << s <<std::endl;

  hc::completion_future ft = hc::parallel_for_each(t_iext,
    [=, &d_hc, &q_hc, &res_hc, &h0](hc::tiled_index<1> idx) [[hc]]
  {
    const unsigned w_id = idx.tile[0]; // Workgroup number maps to which q I use.

    for(unsigned r = 1; r < rows; r++){
      int q_val = q_hc[w_id][r-1];
      unsigned ev = r % 2; // Refers to the row above.
      unsigned od = (r+1) % 2; // Refers to the current row.

      for(unsigned j = 0; j < s; j++){
        const unsigned col = idx.local[0] * s + j + 1;

        int h_nw = h0[w_id][col-1][ev] + score(q_val, d_hc[col-1]);
        int h_w  = h0[w_id][col-1][od] - gap;
        int h_n  = h0[w_id][col][ev] - gap;

        int m;
        m = __max(0, h_nw);
        m = __max(h_n, m);
        m = __max(h_w, m);

        h0[w_id][col][od] = m;
      }

      unsigned j = 0;
      int cmp = 1;
      while(hc::__ballot(cmp) != 0){
      // while(false){
        const unsigned col = idx.local[0] * s + j + 1;
        int imm_l = h0[w_id][col-1][od] - gap;
        int imm_c = h0[w_id][col][od];
        cmp = (imm_l > imm_c) ? 1 : 0;
        if(cmp){
          h0[w_id][col][od] = imm_l;
        }
        j++;
        if(j == s){
          j=0;
        }
      }
      int local_max = 0;
      for(unsigned j = 0; j < s; j++){
        const unsigned col = idx.local[0] * s + j + 1;
        // TODO parallel reduction in LDS
        local_max =__max(h0[w_id][col][od], local_max);
        h0[w_id][col][ev] = 0;
      }
      hc::atomic_fetch_max(&res_hc[w_id], local_max);
    }
  });
  // uint64_t diff = ft.get_end_tick() - ft.get_begin_tick();
  // double exec_time = (double)diff / (double)ft.get_tick_frequency();
  // while(!ft.is_ready()){}
  // std::cout << "Took: "<< diff << std::endl;

  hc::copy(res_hc, result.begin());
}

void swgpu::gen_scores_many_r1(
  const std::string& d,
  const std::array<std::string, CU>& qs,
  const int match,
  const int mismatch,
  const int gap,
  std::array<int, CU>& result
){

  auto arrs = swgpu::prep_arrays_short(d, qs);
  hc::array<short, 1>& d_hc = std::get<0>(arrs);
  hc::array<short, 2>& q_hc = std::get<1>(arrs);
  const unsigned rows = q_hc.get_extent()[1]+1;
  const unsigned cols = d_hc.get_extent()[0]+1;
  assert(rows == qs[0].length()+1);
  assert(cols == d.length()+1);
  unsigned s = (cols-1) / WAVEFRONT_SIZE;
  hc::array<int, 1> res_hc(CU);

  hc::extent<1> iext(CU * WAVEFRONT_SIZE);
  hc::tiled_extent<1> t_iext = iext.tile(WAVEFRONT_SIZE);

  auto score = [=] (short a, short b) [[hc]] {return (a == b) ? match : mismatch;};
  hc::completion_future ft = hc::parallel_for_each(t_iext,
    [=, &d_hc, &q_hc, &res_hc](hc::tiled_index<1> idx) [[hc]]
  {
    static constexpr size_t LDS_SIZE = 2048;
    tile_static short hx[LDS_SIZE][2]; // LDS alloc size must be known at compile time.

    for(unsigned j = 0; j < LDS_SIZE/WAVEFRONT_SIZE; j++){
      size_t id = j+idx.local[0]*(LDS_SIZE/WAVEFRONT_SIZE);
      hx[id][0] = 0;
      hx[id][1] = 0;
    }
    const unsigned w_id = idx.tile[0]; // Workgroup number maps to which q I use.

    for(unsigned r = 1; r < rows; r++){
      short q_val = q_hc[w_id][r-1];
      unsigned ev = r % 2; // Refers to the row above.
      unsigned od = (r+1) % 2; // Refers to the current row.

      for(unsigned j = 0; j < s; j++){
        const unsigned col = idx.local[0] * s + j + 1;

        short h_nw = hx[col-1][ev] + score(q_val, d_hc[col-1]);
        short h_w  = hx[col-1][od] - gap;
        short h_n  = hx[col][ev] - gap;

        short m;
        m = __max(0, h_nw);
        m = __max(h_n, m);
        m = __max(h_w, m);

        hx[col][od] = m;
      }

      unsigned j = 0;
      short cmp = 1;
      while(hc::__any(cmp) != 0){
      // while(false){
        const unsigned col = idx.local[0] * s + j + 1;
        short imm_l = hx[col-1][od] - gap;
        short imm_c = hx[col][od];
        cmp = (imm_l > imm_c) ? 1 : 0;
        if(cmp){
          hx[col][od] = imm_l;
        }
        j++;
        if(j == s){
          j=0;
        }
      }
      int local_max = 0;
      for(unsigned j = 0; j < s; j++){
        const unsigned col = idx.local[0] * s + j + 1;
        local_max =__max(hx[col][od], local_max);
        hx[col][ev] = 0;
      }
      hc::atomic_fetch_max(&res_hc[w_id], local_max);
    }
  });
  hc::copy(res_hc, result.begin());
  uint64_t diff = ft.get_end_tick() - ft.get_begin_tick();
  double exec_time = (double)diff / (double)ft.get_tick_frequency();
  while(!ft.is_ready()){}
  std::cout << "Took: "<< exec_time << std::endl;

}

/*
void swgpu::triangle_kernel(
  const std::string& d,
  const std::array<std::string, CU>& qs,
  const int match,
  const int mismatch,
  const int gap,
  std::array<int, CU>& result
){
  auto arrs = swgpu::prep_arrays_tri(d, qs);
  hc::array<int, 1>& d_hc = std::get<0>(arrs);
  hc::array<int, 2>& q_hc = std::get<1>(arrs);

  // Sizes for grid computation,
  const unsigned rows = q_hc.get_extent()[1];
  const unsigned cols = d_hc.get_extent()[0];

  // Check data sizes...
  assert(rows == qs[0].length());
  assert(cols == d.length()+(WAVEFRONT_SIZE-1)*2);
  // assert(0 == rows % WAVEFRONT_SIZE);

  // Bottom value of diagonal wavefront passes value to next row down.
  // Every CU needs its own row buffer
  std::vector<int> vtop(cols * CU,0);
  hc::extent<2> top_ext(CU, cols);
  hc::array<int, 2> top(top_ext, vtop.begin(), vtop.end());

  // Put the per sequence maxima here.
  std::vector<int> rc(CU,0);
  hc::array<int, 1> res(CU, rc.begin(), rc.end());

  hc::extent<1> ext(CU * WAVEFRONT_SIZE);
  hc::tiled_extent<1> t_ext = ext.tile(WAVEFRONT_SIZE);
  auto score = [=] (int a, int b) [[hc]] {return (a == b) ? match : mismatch;};
  auto is_match = [=] (int a, int b) [[hc]] {return (a == b) ? 9 : 1;};

  // std::cout<<"s: " << s <<std::endl;

  std::vector<int> vdbg(CU * rows * cols, 0);
  hc::extent<3> dbg_ext(CU, rows, cols);
  hc::array<int, 3> dbg(dbg_ext, vdbg.begin(), vdbg.end());

  auto ix = [=] (int a) [[hc]] {return (a % (WAVEFRONT_SIZE+1));};
  hc::completion_future ft = hc::parallel_for_each(t_ext,
    [=, &d_hc, &q_hc, &res, &top, &dbg](hc::tiled_index<1> idx) [[hc]]
  {
    // Holds the 2 most recent columns in the stairstep shape.
    // [0] is on the left, [1] is on the right
    tile_static int w[WAVEFRONT_SIZE+1][2];
    // Holds the 64 immediate values of q seq for fast access.
    tile_static int q_buffer[WAVEFRONT_SIZE];
    //The workgroup number
    int wid = idx.tile[0];
    const int id = idx.local[0];

    for(int r = 0; r < rows; r+=WAVEFRONT_SIZE){
      // Cache the group of 64 Q values.
      q_buffer[id] = q_hc[wid][r+id];
      // Zero out the LDS
      w[id][0] = 0;
      w[id][1] = 0;
      // Pull in first 2 values from row buffer.
      // the "-1" index is mapped to the 65th value, or index '64'
      if(idx.local[0] == 0){
        w[ix(-1)][0] = top[wid][WAVEFRONT_SIZE];
        w[ix(-1)][1] = top[wid][WAVEFRONT_SIZE+1];
      }

      int local_max = -1;
      for(int c = 0; c < cols-(WAVEFRONT_SIZE)+1; c+=1){
      // for(size_t c = 0; c < 2; c+=1){
        // Offset is to create the stairstep pattern
        const int cofst = c + WAVEFRONT_SIZE - id + 1;
        // Load from LDS and perform max reduction
        int h_n  = w[ix(id-1)][1] - gap;
        int h_w  = w[id][1] - gap;
        int h_nw = w[ix(id-1)][0] + score(q_buffer[id], d_hc[cofst]);
        int m1,m2,m;
        m1 = __max(h_n,h_w);
        m2 = __max(h_nw,0);
        m  = __max(m1,m2);
        local_max = __max(local_max, m);

        if(id == 0){
          w[ix(-1)][0] = w[ix(-1)][1]; //shift over
          w[ix(-1)][1] = top[wid][cofst+1]; // store result
        }
        w[id][0] = w[id][1]; //shift over
        w[id][1] = m; // store result

        // This whole section seems bugged.
        if(id == WAVEFRONT_SIZE-1){
          // Bottom lane moves to row buffer.r
          top[wid][c] = m;
        }
        // dbg[wid][id][cofst] = m;

      } //END cols
      hc::atomic_fetch_max(&res[wid], local_max);

    } //END rows

  }); // END parallel_for_each
  while(!ft.is_ready()){}
  hc::copy(res, result.begin());
  // hc::copy(dbg, vdbg.begin());
  // hc::copy(top, vtop.begin());

  bool print = false;
  if(print){
    std::cout << "DBG array" <<std::endl;
    for(size_t i = 0; i < rows; i++){
      std::cout << "[";
      for(size_t j = 63; j < cols-63; j++){
        std::cout << " "<< vdbg[i*cols+j];
        if( (vdbg[i*cols+j] < 10) ){
          std::cout <<" ";
        }
      }
      std::cout << "]" << std::endl;
    }

    std::cout <<"Exiting Row: " << std::endl;
    std::cout << "[";
    for(size_t i = 0; i < cols; i++){
      std::cout << vtop[i] << " ";
    }
    std::cout << "]" << std::endl;
  }
}

*/
