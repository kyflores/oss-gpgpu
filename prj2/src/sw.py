import numpy as np
import sw_data as da
import time

def gen_scores_lin(db_seq, q_seq, fn_sub, Wgp, dbg = False):
    # fn_sub is  / int fn_sub(a,b) /
    H = np.zeros((len(q_seq)+1, len(db_seq)+1), dtype = np.int32)
    orig = np.zeros((len(q_seq)+1, len(db_seq)+1), dtype = np.int8)
    for col in range(1, len(db_seq)+1):
        for row in range(1, len(q_seq)+1):
            # Compute the interior values
            H_nw = H[row-1,col-1] + fn_sub(db_seq[col-1], q_seq[row-1])
            H_n = H[row,col-1] - Wgp
            H_w = H[row-1,col] - Wgp
            H[row,col] = np.amax((H_nw, H_n, H_w, 0))
            orig[row,col] = np.argmax((H_nw, H_n, H_w, 0))

            if dbg:
                ops = (H[row-1,col-1], H[row,col-1], H[row-1,col],0)
                print(str(ops)+"->"+str(ops[orig[row,col]])+"->"+str(H[row, col]))
    return (H, orig)

def gen_scores_wgt(db_seq, q_seq, fn_sub, Ginit, Gext, dbg = False):
    # The main scorre matrix
    H = np.zeros((len(q_seq)+1, len(db_seq)+1), dtype = np.int32)
    # For gaps in rows
    E = np.zeros((len(q_seq)+1, len(db_seq)+1), dtype = np.int32)
    # For gaps in cols
    F = np.zeros((len(q_seq)+1, len(db_seq)+1), dtype = np.int32)
    # Stores origin information for traceback step
    orig = np.zeros((len(q_seq)+1, len(db_seq)+1), dtype = np.int8)

    for col in range(1, len(db_seq)+1):
        for row in range(1, len(q_seq)+1):
            # Compute the interior values
            H_nw = H[row-1,col-1] + fn_sub(db_seq[col-1], q_seq[row-1])
            _e = ((E[row][col-1] - Gext), (H[row][col-1]-Ginit))
            _f = ((F[row-1][col] - Gext), (H[row-1][col]-Ginit))
            E[row,col] = np.amax(_e)
            F[row,col] = np.amax(_f)
            H[row,col] = np.amax((H_nw, E[row,col], F[row,col], 0))
            orig[row,col] = np.argmax((H_nw, E[row,col], F[row,col], 0))

            if dbg:
                ops = (H[row-1,col-1], H[row,col-1], H[row-1,col],0)
                print(str(ops)+"->"+str(ops[orig[row,col]])+"->"+str(H[row, col]))

    return (H, orig)

def traceback(db_seq, q_seq, H, orig):
    gm = np.argmax(H)
    (r,c) = np.unravel_index(gm, H.shape)
    db_align = ""
    q_align = ""
    ctrl = 1000000
    while(ctrl > 0):
        if orig[r,c] == 1: # north
            db_align = db_seq[c-1] + db_align
            q_align = "-" + q_align
            c = c-1
            ctrl = H[r,c]

        elif orig[r,c] == 0: # north west
            db_align = db_seq[c-1] + db_align
            q_align = q_seq[r-1] + q_align
            r = r-1
            c = c-1
            ctrl = H[r,c]

        elif orig[r,c] == 2: # west
            db_align = "-" + db_align
            q_align = q_seq[r-1] + q_align
            r = r-1
            ctrl = H[r,c]

        elif orig[r,c] == 3: # 0. Break
            ctrl = 0

    return (db_align, q_align)

def solve(db_seq, q_seq, fn_sub, Ginit, Gext = 0):
    # Method = 'L' : Linear gap penalty. 'W' : Weighted for open and extend
    if Gext == 0:
        Gext = Ginit
    t1 = time.clock()
    (H, orig) = gen_scores_wgt(db_seq, q_seq, fn_sub, Ginit, Gext)
    t2 = time.clock()
    delta_t = t2-t1
    cells = len(db_seq)*len(q_seq)
    print(str(cells//delta_t)+" cell updates per second.")
    print(np.max(H))

    return traceback(db_seq, q_seq, H, orig)


def test0(): # Do some small seq for debug purposes.
    def fn_sub(a,b):
        return 3 if (a == b) else -3

    # db_seq = "TAGCCCTATCGGTCAA"
    # qs = "TACGGGCCCGCTAC"

    db_seq = "TCCGTAGC"
    qs = "TACTGAGC"
    (H,orig) = gen_scores_wgt(db_seq, qs, fn_sub, 1,1)
    print(H)
    print(orig)
    (d,q) = solve(db_seq,qs,fn_sub,1,1)
    print("Database alignment")
    print(d)
    print("Query alignment")
    print(q)
    print(np.max(H))

def test1(): #run some actual seqs. higher s_ is less similar to s1.
    def fn_sub(a,b):
        return 5 if (a == b) else -4

    def ex_sub(a,b):
        return 3 if (a == b) else -3

    db_seq = da.s1
    qs = [da.s2,da.s3,da.s4,da.s5,da.s6,da.s7]
    counter = 2
    for seq in qs:
        print("Testing query seq " + str(counter))
        counter = counter + 1
        (d,q) = solve(db_seq,seq,fn_sub,1)
        print("Database alignment")
        print(d)
        print("Query alignment")
        print(q)


if __name__ == '__main__':
    test0()
