#include "sw_util.h"

std::vector<short> util::seq_to_vec(const std::string& seq){
  size_t len = seq.length(); // Ignore null terminator.
  std::vector<short> res(len,0);
  for(uint32_t i = 0; i < len; i++){
    switch(seq[i]){
      case 'A': res[i] = 0; break;
      case 'T': res[i] = 1; break;
      case 'G': res[i] = 2; break;
      case 'C': res[i] = 3; break;
      case 'S': res[i] = 4; break;
      case 'W': res[i] = 5; break;
      case 'R': res[i] = 6; break;
      case 'Y': res[i] = 7; break;
      case 'K': res[i] = 8; break;
      case 'M': res[i] = 9; break;
      case 'B': res[i] = 10; break;
      case 'V': res[i] = 11; break;
      case 'H': res[i] = 12; break;
      case 'D': res[i] = 13; break;
      case 'N': res[i] = 14; break;
      default: res[i] = 255;
    }
  }
  return res;
}

std::vector<int> util::seq_to_vec_int(const std::string& seq){
  size_t len = seq.length(); // Ignore null terminator.
  std::vector<int> res(len,0);
  for(unsigned i = 0; i < len; i++){
    switch(seq[i]){
      case 'A': res[i] = 0; break;
      case 'T': res[i] = 1; break;
      case 'G': res[i] = 2; break;
      case 'C': res[i] = 3; break;
      case 'S': res[i] = 4; break;
      case 'W': res[i] = 5; break;
      case 'R': res[i] = 6; break;
      case 'Y': res[i] = 7; break;
      case 'K': res[i] = 8; break;
      case 'M': res[i] = 9; break;
      case 'B': res[i] = 10; break;
      case 'V': res[i] = 11; break;
      case 'H': res[i] = 12; break;
      case 'D': res[i] = 13; break;
      case 'N': res[i] = 14; break;
      default: res[i] = 255;
    }
  }
  return res;
}

void util::reorder_seq(std::vector<int16_t>& vec, size_t lanes, bool cmd){
  assert(vec.size() % lanes == 0);
  size_t seg_len = vec.size() / lanes;
  std::vector<int16_t> tmp(vec.size(), 14); // 14 is the sequence code for N.
  if(cmd){ // Setting cmd = true will "destripe" the input instead.
    size_t tmp = seg_len;
    seg_len = lanes;
    lanes = tmp;
  }
  for(uint32_t l = 0; l < seg_len; l++){
    for(uint32_t i = 0; i < lanes; i++){
      tmp[l * lanes + i] = vec[i*seg_len + l];
    }
  }
  vec = tmp;
}

void util::reorder_seq(std::vector<int>& vec, size_t lanes, bool cmd){
  assert(vec.size() % lanes == 0);
  size_t seg_len = vec.size() / lanes;
  std::vector<int> tmp(vec.size(), 14); // 14 is the sequence code for N.
  if(cmd){ // Setting cmd = true will "destripe" the input instead.
    size_t tmp = seg_len;
    seg_len = lanes;
    lanes = tmp;
  }
  for(unsigned l = 0; l < seg_len; l++){
    for(unsigned i = 0; i < lanes; i++){
      tmp[l * lanes + i] = vec[i*seg_len + l];
    }
  }
  vec = tmp;
}

int util::argmax(std::array<int,4>& arr){
  int m = 0;
  int argm = arr.size()-1;
  for(int i = 0; i < arr.size(); i++){
    argm = (arr[i]>m) ? i : argm;
    m = (arr[i]>m) ? arr[i] : m;
  }
  return argm;
}

std::string util::random_sequence(size_t len, int seed){
  std::srand(seed);
  std::string rval;
  rval.resize(len);
  for(size_t i = 0; i < len; i++){
    int rnd = std::rand();
    if(rnd < RAND_MAX/4){
      rval[i] = 'A';
    }
    else if(rnd < RAND_MAX/2){
      rval[i] = 'T';
    }
    else if(rnd < (int)(RAND_MAX*0.75f)){
      rval[i] = 'G';
    }
    else{
      rval[i] = 'C';
    }
  }
  return rval;
}
