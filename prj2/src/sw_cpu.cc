#include "sw_cpu.h"

inline int16_t __max(int16_t a, int16_t b){
  return (a > b) ? a : b;
}

int16_t swcpu::gen_scores(
  const std::string d,
  const std::string q,
  const int16_t match,
  const int16_t mismatch,
  const int16_t gap,
  std::vector< std::vector<int16_t> >& H,
  std::vector< std::vector<int16_t> >& T
){
  std::vector<int16_t> ds = util::seq_to_vec(d);
  std::vector<int16_t> qs = util::seq_to_vec(q);
  const size_t rows = qs.size()+1;
  const size_t cols = ds.size()+1;

  std::vector< std::vector<int16_t> > H_grid;
  H_grid.resize(rows);
  for(auto &v : H_grid){
    v.resize(cols);
    std::fill(v.begin(), v.end(), 0);
  }
  std::vector< std::vector<int16_t> > T_grid;
  T_grid.resize(rows);
  for(auto &v : T_grid){
    v.resize(cols);
    std::fill(v.begin(), v.end(), 0);
  }

  auto score = [=] (int16_t a, int16_t b) {return (a == b) ? match : mismatch;};
  for (size_t r = 1; r < rows; r++){
    for (size_t c = 1; c < cols; c++){
      std::array<int16_t, 4> cands = {0};
      cands[0] = H_grid[r-1][c-1] + score(qs[r-1],ds[c-1]);
      cands[1] = H_grid[r][c-1] - gap;
      cands[2] = H_grid[r-1][c] - gap;
      cands[3] = 0;

      int16_t m = 0;
      int16_t argm = 3;

      for(int x = 0; x < 4; x++){
          argm = (cands[x]>m) ? x : argm;
          m = (cands[x]>m) ? cands[x] : m;
      }

      H_grid[r][c] = m;
      T_grid[r][c] = argm;
    }
  }

  int16_t reval = 0;
  for(auto &x : H_grid){
    for(auto y : x){
      reval = (y>reval) ? y : reval;
    }
  }

  H = H_grid;
  T = T_grid;

  return reval;
}

int16_t swcpu::traceback(
  const std::string& d,
  const std::string& q,
  std::vector< std::vector<int16_t> >& H,
  std::vector< std::vector<int16_t> >& T,
  std::string& d_al,
  std::string& q_al){

  const size_t rows = q.length()+1;
  const size_t cols = d.length()+1;

  int16_t max = 0;
  size_t r_max, c_max;
  //backwards loop helps find the farthest out maxima if there are several.
  for(int32_t r = rows-1; r >= 0; r--){
    for(int32_t c = cols-1; c >= 0; c--){
      if(H[r][c] > max){
        max = H[r][c];
        r_max = r;
        c_max = c;
      }
    }
  }
  d_al = "";
  q_al = "";

  int32_t ctrl = 1000000;
  while (ctrl > 0) {
    switch(T[r_max][c_max]){
      case 0:
        d_al.insert(0, 1, d[c_max-1]);
        q_al.insert(0, 1, q[r_max-1]);
        r_max -= 1;
        c_max -= 1;
        ctrl = H[r_max][c_max];
        break;

      case 1:
        d_al.insert(0, 1, d[c_max-1]);
        q_al.insert(0, 1, '-');
        c_max -= 1;
        ctrl = H[r_max][c_max];
        break;

      case 2:
        d_al.insert(0, 1, '-');
        q_al.insert(0, 1, q[r_max-1]);
        r_max -= 1;
        ctrl = H[r_max][c_max];
        break;
    }
  }
  // std::cout<<d_al<<std::endl<<q_al<<std::endl;
  return max;
}
