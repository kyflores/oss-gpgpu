#include "doctest.h"
#include "sw_cpu.h"
#include "sw_proto.h"
#include "sw_gpu.h"
#include "sw_test_data.h"
#include <vector>
#include <string>
#include <hcc/hc.hpp>

TEST_CASE("Test properly sized array initialization"){
  std::string ex64 =
    "ATGCATGCATGCATGCATGCATGCATGCATGCATGCATGCATGCATGCATGCATGCATGCATGC";

  std::string d = ex64+ex64;
  std::array<std::string, CU> qs;
  for(auto &i : qs){
    i = ex64;
  }

  auto res = swgpu::prep_arrays_tri(d, qs);
  hc::array<int, 1> d_hc = std::get<0>(res);
  hc::array<int, 2> q_hc = std::get<1>(res);

  // Extract extents to check assertions.
  hc::extent<1> d_extent = d_hc.get_extent();
  hc::extent<2> q_extent = q_hc.get_extent();
  CHECK(d_extent[0] == 128+63+63);
  CHECK(q_extent[0] == CU);
  CHECK(q_extent[1] == 64);

  std::vector<int> dvec(d_extent[0], 0);
  std::vector<int> qvec(q_extent[0]*q_extent[1], 0);
  hc::copy(d_hc, dvec.begin());
  hc::copy(q_hc, qvec.begin());
  CHECK(dvec[0] == -1);
  CHECK(dvec[62] == -1);
  CHECK(dvec[63] == 0);
  CHECK(dvec[64] == 1);
  CHECK(dvec[65] == 2);
  CHECK(dvec[66] == 3);
  CHECK(dvec[67] == 0);

  CHECK(qvec[0] == 0);
  CHECK(qvec[1] == 1);
  CHECK(qvec[2] == 2);
  CHECK(qvec[3] == 3);
  CHECK(qvec[4] == 0);
}


TEST_CASE("Test grid computation on exact match sequence"){
  std::string ex16 = "ATGCATGCATGCATGC";
  std::string ex32 = ex16+ex16;
  std::string ex64 = ex32+ex32;
  std::string ex128 = ex32+ex32+ex32+ex32;

  std::string d = ex16;
  std::array<std::string, CU> qs;
  for(auto &i : qs){
    i = ex64;
  }
  std::array<int, CU> result;
  swgpu::triangle_kernel(d, qs, 5, -4, 1, result);

  // std::vector< std::vector<int16_t> > H;
  // std::vector< std::vector<int16_t> > T;
  // int16_t m = swcpu::gen_scores(ex16,ex64,5,-4,1,H,T);
  //
  // for(size_t i = 1; i < result.size(); i++){
  //   CHECK(result[i] == result[i-1]);
  // }
  // CHECK(m == result[0]);
}
