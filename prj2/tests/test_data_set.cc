#include "doctest.h"
#include "sw_proto.h"
#include "sw_cpu.h"
#include "sw_test_data.h"
#include <hcc/hc.hpp>
#include <chrono>

/*
TEST_CASE("Run small sequences for debug."){

  std::string d = "ATGCATGC";

  // Seq 1 and 2 cause errors.
  std::array<std::string,3> queries({"ATGG","GCATG","CCCCC"});

  REQUIRE(d.length() % SIMD_WIDTH_16  == 0);
  const int16_t match = 5;
  const int16_t mismatch = -4;
  const int16_t gap = 1;
  for(auto q : queries){
    std::vector< std::vector<int16_t> > H_c;
    std::vector< std::vector<int16_t> > H_p;
    std::vector< std::vector<int16_t> > T_c;
    std::vector< std::vector<int16_t> > T_p;

    swcpu::gen_scores(d,q,match,mismatch,gap,H_c,T_c);
    swhc::gen_scores(d,q,match,mismatch,gap,H_p,T_p);

    std::string d_alp = "";
    std::string q_alp = "";
    std::string d_alc = "";
    std::string q_alc = "";
    int16_t maxc, maxp;
    maxc = swcpu::traceback(d,q,H_c,T_c,d_alc,q_alc);
    maxp = swcpu::traceback(d,q,H_p,T_p,d_alp,q_alp);
    CHECK(maxc==maxp);
    uint32_t t_err = 0;
    uint32_t h_err = 0;
    for(size_t r = 0; r < H_c.size(); r++){
      for(size_t c = 0; c < H_c[0].size(); c++){
        if(H_c[r][c] != H_p[r][c]){h_err+=1;}
        if(T_c[r][c] != T_p[r][c]){t_err+=1;}
      }
    }
    CHECK(h_err == 0);
    uint32_t d_err = 0;
    uint32_t q_err = 0;
    for(size_t i = 0; i < d_alp.length(); i++){
      if(d_alp[i] != d_alc[i]){d_err++;}
    }
    for(size_t i = 0; i < q_alp.length(); i++){
      if(q_alp[i] != q_alc[i]){q_err++;}
    }
  }
}
*/

TEST_CASE("Run multiple datasets of serial CPU impl against simulated parallel."){

  // std::string d = "TAGCCCTATCGGTCAA";
  std::string d = s1;

  // std::array<std::string,1> queries({"TACGGGCCCGCTAC"});
  std::array<std::string,6> queries({s2,s3,s4,s5,s6,s7});

  REQUIRE(d.length() % SIMD_WIDTH_16  == 0);
  const int16_t match = 5;
  const int16_t mismatch = -4;
  const int16_t gap = 1;

  for(auto q : queries){

    std::vector< std::vector<int16_t> > H_c;
    std::vector< std::vector<int16_t> > H_p;

    std::vector< std::vector<int16_t> > T_c;
    std::vector< std::vector<int16_t> > T_p;

    std::chrono::high_resolution_clock::time_point start_time;
    std::chrono::high_resolution_clock::time_point end_time;

    start_time = std::chrono::high_resolution_clock::now();
    swcpu::gen_scores(d,q,match,mismatch,gap,H_c,T_c);
    end_time = std::chrono::high_resolution_clock::now();
    double result = std::chrono::duration_cast<std::chrono::microseconds>\
      (end_time - start_time).count();
    std::cout<<"Serial:" << result <<" us"<<" : ";

    start_time = std::chrono::high_resolution_clock::now();
    swhc::gen_scores(d,q,match,mismatch,gap,H_p,T_p);
    end_time = std::chrono::high_resolution_clock::now();
    result = std::chrono::duration_cast<std::chrono::microseconds>\
      (end_time - start_time).count();
    std::cout<<"Vec:" << result <<" us"<<std::endl;

    std::string d_alp = "";
    std::string q_alp = "";

    std::string d_alc = "";
    std::string q_alc = "";

    int16_t maxc, maxp;
    maxc = swcpu::traceback(d,q,H_c,T_c,d_alc,q_alc);
    maxp = swcpu::traceback(d,q,H_p,T_p,d_alp,q_alp);
    CHECK(maxc==maxp);
    std::cout<<"MAX: "<< maxc <<std::endl;

    uint32_t t_err = 0;
    uint32_t h_err = 0;
    for(size_t r = 0; r < H_c.size(); r++){
      for(size_t c = 0; c < H_c[0].size(); c++){
        if(H_c[r][c] != H_p[r][c]){h_err+=1;}
        if(T_c[r][c] != T_p[r][c]){t_err+=1;}
      }
    }
    // CHECK(t_err == 0); #
    CHECK(h_err == 0);


    CHECK(d_alp.length() == d_alc.length());
    CHECK(q_alp.length() == q_alc.length());

    uint32_t d_err = 0;
    uint32_t q_err = 0;
    // for(size_t i = 0; i < d_alp.length(); i++){
    //   if(d_alp[i] != d_alc[i]){d_err++;}
    // }
    //
    // for(size_t i = 0; i < q_alp.length(); i++){
    //   if(q_alp[i] != q_alc[i]){q_err++;}
    // }
    // CHECK(d_err == 0);
    // CHECK(q_err == 0);
  }
}
