#include "doctest.h"
#include "sw_util.h"

TEST_CASE("Test array2d element access"){
  array2d<int32_t> a(3,3);
  a(1,1) = 4;
  CHECK(4 == a(1,1));
}

TEST_CASE("Test unravel index"){
  array2d<int32_t> a(3,3);
  a(1,1) = 9;
  dim2d idx = a.unravel_index(4);
  CHECK( 9 == a(idx.r, idx.c));
}

TEST_CASE("Test construction with an input vector."){
  std::vector<int32_t> v {0,1,2,3,4,5,6,7,8};
  array2d<int32_t> a(3,3,v);
  CHECK(a(1,1) == 4);
}

TEST_CASE("Test sequence to integer encoding"){
  std::string test = "ATGCCGTA";
  std::array<int16_t,8> ver({0,1,2,3,3,2,1,0});
  std::vector<int16_t> res = util::seq_to_vec(test);
  bool err = false;
  for(size_t i = 0; i < ver.size(); i++){
    CHECK(ver[i]==res[i]);
  }
}

TEST_CASE("Test fwd striped reordering"){
  std::string test = "ATGCATGCATGC";
  std::vector<int16_t> res = util::seq_to_vec(test);
  size_t lanes = 3;
  util::reorder_seq(res, lanes, false);
  CHECK(res[0] == 0);
  CHECK(res[2] == 0);
  CHECK(res[3] == 1);
  CHECK(res[5] == 1);
  CHECK(res[6] == 2);
  CHECK(res[8] == 2);
  CHECK(res[9] == 3);
  CHECK(res[11] == 3);
}

TEST_CASE("Test destripe reordering"){
  std::string test = "AAATTTGGGCCC";
  std::vector<int16_t> res = util::seq_to_vec(test);
  size_t lanes = 3;
  util::reorder_seq(res, lanes, true);
  CHECK(res[0] == 0);
  CHECK(res[1] == 1);
  CHECK(res[2] == 2);
  CHECK(res[3] == 3);
}
