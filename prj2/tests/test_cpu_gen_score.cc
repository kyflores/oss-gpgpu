#include "doctest.h"
#include "sw_util.h"
#include "sw_cpu.h"
#include <string>
#include <vector>
#include <chrono>

//Later, using http://rosalind.info/glossary/dnafull/

TEST_CASE("Generate a scoring matrix, and trace it."){
  std::string ex16 = "ATGCATGCATGCATGC";
  std::string ex32 = ex16+ex16;
  std::string ex64 = ex32+ex32;
  std::string d = ex16;
  std::string q = ex64;
  const int16_t match = 5;
  const int16_t mismatch = -4;
  const int16_t gap = 1;
  std::vector< std::vector<int16_t> > H;
  std::vector< std::vector<int16_t> > T;
  int16_t m = swcpu::gen_scores(d,q,match,mismatch,gap,H,T);
  std::cout << "MAX:" << m << std::endl;
  std::string d_al = "";
  std::string q_al = "";
  swcpu::traceback(d,q,H,T,d_al,q_al);
  // Print the results.
  for(size_t i = 0; i < H.size(); i++){
    std::cout << "[";
    for(size_t j = 0; j < H[0].size(); j++){
      std::cout << " "<< H[i][j];
      if( H[i][j] < 10){
        std::cout <<" ";
      }
    }
    std::cout << "]" << std::endl;
  }
  //
  // for(size_t i = 0; i < T.size(); i++){
  //   std::cout << "[";
  //   for(size_t j = 0; j < T[0].size(); j++){
  //     std::cout << T[i][j] << " ";
  //   }
  //   std::cout << "]" << std::endl;
  // }

}
