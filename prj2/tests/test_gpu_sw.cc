#include "doctest.h"
#include "sw_cpu.h"
#include "sw_proto.h"
#include "sw_gpu.h"
#include "sw_test_data.h"
#include <vector>
#include <string>
#include <hcc/hc.hpp>

TEST_CASE("Spit out string sizes"){
  std::cout<<"S1:" << s1.length() << std::endl;
  std::cout<<"S2:" << s2.length() << std::endl;
  std::cout<<"S3:" << s3.length() << std::endl;
  std::cout<<"S4:" << s4.length() << std::endl;
  std::cout<<"S5:" << s5.length() << std::endl;
  std::cout<<"S6:" << s6.length() << std::endl;
  std::cout<<"S7:" << s7.length() << std::endl;
}

TEST_CASE("How big are different ints"){
  std::cout<< "char:" << sizeof(char) << std::endl;
  std::cout<< "int:" << sizeof(int) << std::endl;
  std::cout<< "unsigned:" << sizeof(unsigned) << std::endl;
  std::cout<< "long:" << sizeof(long) << std::endl;
  std::cout<< "short:" << sizeof(short) << std::endl;
}

TEST_CASE("Test properly sized array initialization"){
  std::string ex64 =
    "ATGCATGCATGCATGCATGCATGCATGCATGCATGCATGCATGCATGCATGCATGCATGCATGC";

  std::string d = ex64+ex64;
  std::array<std::string, CU> qs;
  for(auto &i : qs){
    i = ex64;
  }

  auto res = swgpu::prep_arrays(d, qs);
  hc::array<int, 1> d_hc = std::get<0>(res);
  hc::array<int, 2> q_hc = std::get<1>(res);

  // Extract extents to check assertions.
  hc::extent<1> d_extent = d_hc.get_extent();
  hc::extent<2> q_extent = q_hc.get_extent();
  CHECK(d_extent[0] == 128);
  CHECK(q_extent[0] == CU);
  CHECK(q_extent[1] == 64);

  std::vector<int> dvec(d_extent[0], 0);
  std::vector<int> qvec(q_extent[0]*q_extent[1], 0);
  hc::copy(d_hc, dvec.begin());
  hc::copy(q_hc, qvec.begin());
  CHECK(dvec[0] == 0);
  CHECK(dvec[1] == 1);
  CHECK(dvec[2] == 2);
  CHECK(dvec[3] == 3);
  CHECK(dvec[4] == 0);

  CHECK(qvec[0] == 0);
  CHECK(qvec[1] == 1);
  CHECK(qvec[2] == 2);
  CHECK(qvec[3] == 3);
  CHECK(qvec[4] == 0);
}

TEST_CASE("Test grid computation on exact match sequence"){
  std::string ex16 = "ATGCATGCATGCATGC";
  std::string ex32 = ex16+ex16;
  std::string ex64 = ex32+ex32;
  std::string ex128 = ex32+ex32+ex32+ex32;

  std::string d = ex128;
  std::array<std::string, CU> qs;
  for(auto &i : qs){
    i = ex64;
  }
  std::array<int, CU> result;
  swgpu::gen_scores_many(d, qs, 5, -4, 1, result);

  std::vector< std::vector<int16_t> > H;
  std::vector< std::vector<int16_t> > T;
  int16_t m = swcpu::gen_scores(ex128,ex64,5,-4,1,H,T);

  for(size_t i = 1; i < result.size(); i++){
    CHECK(result[i] == result[i-1]);
  }
  CHECK(m == result[0]);
}

TEST_CASE("Test grid computation on nontrivial sequence"){
  std::string d = s1.substr(0,1024);
  std::string q = s2.substr(0,128);
  std::array<std::string, CU> qs;
  for(auto &i : qs){
    i = q;
  }
  std::array<int, CU> result;
  swgpu::gen_scores_many_r1(d, qs, 5, -4, 1, result);

  std::vector< std::vector<int16_t> > H;
  std::vector< std::vector<int16_t> > T;
  int16_t m = swcpu::gen_scores(d,q,5,-4,1,H,T);

  for(size_t i = 1; i < result.size(); i++){
    CHECK(result[i] == result[i-1]);
  }
  CHECK(m == result[0]);
}


// TEST_CASE("Load the system with long, random sequences, and check with CPU."){
//   std::string d = util::random_sequence(2<<10, 100);
//   std::array<std::string, CU> qs;
//   std::cout << "Making random sequences..."<<std::endl;
//   const int a = 5, b = -4, c = 1;
//
//   int count = 1;
//   for(auto &i : qs){
//     // i = d;
//     i = util::random_sequence(2<<10, count);
//     count++;
//   }
//   std::array<int, CU> result;
//   auto start_time = std::chrono::high_resolution_clock::now();
//   swgpu::gen_scores_many_r1(d, qs, a, b, c, result);
//   auto end_time = std::chrono::high_resolution_clock::now();
//   double gpu_time = std::chrono::duration_cast<std::chrono::microseconds>\
//     (end_time - start_time).count();
//   double cpu_time = 0;
//
//   std::vector< std::vector<int16_t> > H;
//   std::vector< std::vector<int16_t> > T;
//   for(size_t i = 0; i < qs.size(); i++){
//     start_time = std::chrono::high_resolution_clock::now();
//     int16_t m = swhc::gen_scores(d,qs[i],a,b,c,H,T);
//     end_time = std::chrono::high_resolution_clock::now();
//     cpu_time +=std::chrono::duration_cast<std::chrono::microseconds>\
//       (end_time - start_time).count();
//     CHECK(m == result[i]);
//     int16_t error = result[i] - m;
//     std::cout << error << ", ";
//   }
//   std::cout << std::endl;
//   std::cout << "GPU took " << gpu_time/1000000 << '\n';
//   std::cout << "CPU took " << cpu_time/1000000 << '\n';
// }


TEST_CASE("Run real test data."){
  std::string d = s1.substr(0,1024);
  std::array<std::string, CU> qs;
  const int a = 5, b = -4, c = 5;

  for(size_t i = 0; i < CU; i++){
    switch(i%6){
      case 0:
        qs[i] = s2.substr(0,1024);
        break;
      case 1:
        qs[i] = s3.substr(0,1024);
        break;
      case 2:
        qs[i] = s4.substr(0,1024);
        break;
      case 3:
        qs[i] = s5.substr(0,1024);
        break;
      case 4:
        qs[i] = s6.substr(0,1024);
        break;
      case 5:
        qs[i] = s7.substr(0,1024);
        break;
      default:
        qs[i] = s1.substr(0,1024);

    }
  }
  std::array<int, CU> result;
  auto start_time = std::chrono::high_resolution_clock::now();
  swgpu::gen_scores_many_r1(d, qs, a, b, c, result);
  auto end_time = std::chrono::high_resolution_clock::now();
  double gpu_time = std::chrono::duration_cast<std::chrono::microseconds>\
    (end_time - start_time).count();
  double cpu_time = 0;

  std::vector< std::vector<int16_t> > H;
  std::vector< std::vector<int16_t> > T;
  for(size_t i = 0; i < qs.size(); i++){
    start_time = std::chrono::high_resolution_clock::now();
    int16_t m = swhc::gen_scores(d,qs[i],a,b,c,H,T);
    end_time = std::chrono::high_resolution_clock::now();
    cpu_time +=std::chrono::duration_cast<std::chrono::microseconds>\
      (end_time - start_time).count();
    CHECK(m == result[i]);
    int16_t error = result[i] - m;
    std::cout << error << ", ";
  }
  std::cout << std::endl;
  std::cout << "GPU took " << gpu_time/1000000 << '\n';
  std::cout << "CPU took " << cpu_time/1000000 << '\n';
  std::cout << "Speedup of " << (cpu_time/gpu_time) << " times.\n";
}
