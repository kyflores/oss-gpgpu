#include "doctest.h"
#include "sw_proto.h"
#include "sw_cpu.h"
#include "sw_test_data.h"
#include <vector>
#include <hcc/hc.hpp>

TEST_CASE("Test the vectorize example kernel."){

  std::vector<int32_t> X(100, 0);
  std::vector<int32_t> Y(100, 0);
  for(uint32_t i = 0; i < X.size(); i++){
    X[i] = i;
  }
  swhc::vec_test(Y,X,2,0);
  CHECK(Y[99] == 2*99);
}

TEST_CASE("Test sequence to integer encoding"){
  std::string test = "ATGCCGTA";
  std::array<int16_t,8> ver({0,1,2,3,3,2,1,0});
  std::vector<int16_t> res = util::seq_to_vec(test);
  bool err = false;
  for(size_t i = 0; i < ver.size(); i++){
    CHECK(ver[i]==res[i]);
  }
}

TEST_CASE("Test fwd striped reordering"){
  std::string test = "ATGCATGCATGC";
  std::vector<int16_t> res = util::seq_to_vec(test);
  size_t lanes = 3;
  util::reorder_seq(res, lanes, false);
  CHECK(res[0] == 0);
  CHECK(res[2] == 0);
  CHECK(res[3] == 1);
  CHECK(res[5] == 1);
  CHECK(res[6] == 2);
  CHECK(res[8] == 2);
  CHECK(res[9] == 3);
  CHECK(res[11] == 3);
}

TEST_CASE("Test destripe reordering"){
  std::string test = "AAATTTGGGCCC";
  std::vector<int16_t> res = util::seq_to_vec(test);
  size_t lanes = 3;
  util::reorder_seq(res, lanes, true);
  CHECK(res[0] == 0);
  CHECK(res[1] == 1);
  CHECK(res[2] == 2);
  CHECK(res[3] == 3);
}

TEST_CASE("Test row processing with entering 0s"){
  std::string sequence = "ATGCATGCATGCATGC";
  std::vector<int16_t> i_seq = util::seq_to_vec(sequence);
  size_t lanes = 4;
  std::vector<int16_t> h_top(i_seq.size()+1, 0);

  int16_t q_value = 0; // A
  std::vector<int16_t> H(h_top); // copy to get same size.
  std::vector<int16_t> T(h_top);
  const int16_t match = 5;
  const int16_t mismatch = -4;
  const int16_t gap = 1;

  swhc::process_row(i_seq, h_top, q_value, H, T, match, mismatch, gap);

  // for(auto h : H){
  //   std::cout << h << ", ";
  // }
  // std::cout << std::endl;
  CHECK(H[1] == 5);
  CHECK(H[2] == 4);
  CHECK(H[3] == 3);
  CHECK(H[4] == 2);
  CHECK(H[5] == 5);
  CHECK(H[9] == 5);

  CHECK(T[2] == 1);
  CHECK(T[3] == 1);
  CHECK(T[4] == 1);
  CHECK(T[5] == 0);
}

TEST_CASE("Test row processing with incoming non-zero row."){
  std::string sequence = "ATGCATGCATGCATGC";
  std::vector<int16_t> i_seq = util::seq_to_vec(sequence);
  size_t lanes = 4;
  std::vector<int16_t> h_top({0,4,10,9,8,7,10,9,8,7,10,9,8,7,10,9,8});

  int16_t q_value = 2; // G
  std::vector<int16_t> H(h_top); // copy to get same size.
  std::vector<int16_t> T(h_top);
  const int16_t match = 5;
  const int16_t mismatch = -4;
  const int16_t gap = 1;

  swhc::process_row(i_seq, h_top, q_value, H, T, match, mismatch, gap);
  // for(auto h : H){
  //   std::cout << h << ", ";
  // }
  // std::cout << std::endl;

  CHECK(H[0] == 0);
  CHECK(H[1] == 3);
  CHECK(H[2] == 9);
  CHECK(H[3] == 15);
}

/*
TEST_CASE("Print out the scoring matrix for two sequences."){
  // std::string d = "TAGCCCTATCGGTCAA";
  // std::string q = "TACGGGCCCGCTAC";
  std::string d = "ATGCATGC";
  std::string q = "ATGG";
  const int16_t match = 5;
  const int16_t mismatch = -4;
  const int16_t gap = 1;
  std::vector< std::vector<int16_t> > H;
  std::vector< std::vector<int16_t> > T;
  swhc::gen_scores(d,q,match,mismatch,gap,H,T);
  std::string d_al = "";
  std::string q_al = "";
  swcpu::traceback(d,q,H,T,d_al,q_al);
  // Print the results.
  for(size_t i = 0; i < H.size(); i++){
    std::cout << "[";
    for(size_t j = 0; j < H[0].size(); j++){
      std::cout << " "<< H[i][j];
      if( H[i][j] < 10){
        std::cout <<" ";
      }
    }
    std::cout << "]" << std::endl;
  }

  for(size_t i = 0; i < T.size(); i++){
    std::cout << "[";
    for(size_t j = 0; j < T[0].size(); j++){
      std::cout << T[i][j] << " ";
    }
    std::cout << "]" << std::endl;
  }
}
*/

TEST_CASE("Test the 'max only' implementation on dbg data."){
  const int16_t match = 5;
  const int16_t mismatch = -4;
  const int16_t gap = 1;
  int16_t maximum = 0;
  std::string d = "ATGCATGCATGCATGC";
  std::string q = "ATGGATGGATGGATGG";
  maximum = swhc::gen_max_score(d,q,match,mismatch,gap);
  CHECK(maximum == 54);
}


TEST_CASE("Test the 'max only' implementation on real data."){
  const int16_t match = 5;
  const int16_t mismatch = -4;
  const int16_t gap = 1;
  int16_t maximum = 0;
  maximum = swhc::gen_max_score(s1,s2,match,mismatch,gap);
  CHECK(maximum == 5681);
  maximum = swhc::gen_max_score(s1,s3,match,mismatch,gap);
  CHECK(maximum == 3774);
  maximum = swhc::gen_max_score(s1,s4,match,mismatch,gap);
  CHECK(maximum == 4459);
  maximum = swhc::gen_max_score(s1,s5,match,mismatch,gap);
  CHECK(maximum == 3170);
  maximum = swhc::gen_max_score(s1,s6,match,mismatch,gap);
  CHECK(maximum == 3662);
  maximum = swhc::gen_max_score(s1,s7,match,mismatch,gap);
  CHECK(maximum == 3793);
}
