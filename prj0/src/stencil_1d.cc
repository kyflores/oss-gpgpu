#include <vector>
#include <array>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <chrono>

#include "hcc/hc.hpp"
// #include <CXLActivityLogger.h>

void window3_gpu(
  std::vector<float>& a,
  const std::array<float,3>& b,
  std::vector<float>& c){

  std::cout << "Launch GPU kernel.\n";
  // Construct an extent object to be 1D and the size of a.
  // Extents represent the full problem space.
  hc::extent<1> ext(a.size());

  // array_views of a and c manage data transfer of the host arrays.
  hc::array_view<float, 1> v_a(ext, a.data());
  hc::array_view<float, 1> v_c(ext, c.data());
  const int iter = a.size()-2;

  float k0 = b[0];
  float k1 = b[1];
  float k2 = b[2];

  // GPU kernel captures k0-k2 by value.
  // hc::index is the sole argument to the kernel, that gibes
  // information to the thread as to its position in the
  // global extent.

  auto start = std::chrono::high_resolution_clock::now();
  hc::parallel_for_each(hc::extent<1> (iter), [=] (hc::index<1> i) [[hc]] {
      v_c[i] = k0*v_a[i] + k1*v_a[i+1] + k2*v_a[i+2];
  });

  auto end = std::chrono::high_resolution_clock::now();

  std::cout << "Execution took "
              << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
              << "us.\n";
}

void window3_cpu(
  const std::vector<float>& a,
  const std::array<float,3>& b,
  std::vector <float>& c){

  std::cout << "Launch CPU kernel.\n";

  auto start = std::chrono::high_resolution_clock::now();
  for(int i = 0; i < (a.size()-2); i++){
    c[i] = b[0] * a[i] + b[1] * a[i+1] + b[2] * a[i+2];
    // c[i] = 1;
  }
  auto end = std::chrono::high_resolution_clock::now();
  std::cout << "Execution took "
              << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
              << "us.\n";
}


int main(){
  constexpr uint problem_size = (1<<8);

  std::vector<float> a(problem_size , 0);
  std::array<float, 3> b = {0.333f, 0.333f, 0.333f};
  std::vector<float> c_cpu(problem_size , 0);
  std::vector<float> c_gpu(problem_size , 0);

  //Populate array 0->N
  for(int i = 0; i < a.size(); i++){a[i] = std::sin(i);}

  if(problem_size <= 1<<8){
    std::cout << "Starting Array:\n";
    std::copy(a.begin(),
              a.end(),
              std::ostream_iterator<float>(std::cout,",")
              );
    std::cout << "\n\n";
  }

  window3_cpu(a, b, c_cpu);
  if(problem_size <= 1<<8){
    std::copy(c_cpu.begin(),
              c_cpu.end(),
              std::ostream_iterator<float>(std::cout,",")
              );
    std::cout << "\n\n";
  }

  window3_gpu(a, b, c_gpu);
  if(problem_size <= 1<<8){
    std::copy(c_gpu.begin(),
              c_gpu.end(),
              std::ostream_iterator<float>(std::cout,",")
              );
    std::cout << "\n\n";
  }

  int errors = 0;
  for (int i = 0; i < c_cpu.size(); i++) {
    if (fabs(c_cpu[i] - c_gpu[i]) > fabs(c_cpu[i] * 0.0001f))
      errors++;
  }
  std::cout << errors << " errors" << std::endl;
}
