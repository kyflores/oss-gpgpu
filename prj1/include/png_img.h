#ifndef __PNG_IMG_H
#define __PNG_IMG_H

#include "lodepng.h"
#include <iostream>
#include <string>
#include <vector>
#include <cstdint>

class PNGimg {
public:
  typedef struct{
    float R;
    float G;
    float B;
  }GrayScaleWeightStruct;

  typedef struct{
    size_t width;
    size_t height;
  }ImgDimStruct;

  PNGimg(std::string _file, uint32_t _width, uint32_t _height) :
    file(_file), width(_width), height(_height){}

  std::vector<uint8_t> decode_to_arr();
  void encode_from_arr(std::vector<uint8_t>& data, std::string append = ".out");
  void decode();
  void encode();
  // Returns array with one intensity value per pixel
  std::vector<uint8_t> get_grayscale();
  // Generates redundant RGBA channels for the encode() function
  std::vector<uint8_t> save_grayscale(std::vector<uint8_t>& gray_img);

  ImgDimStruct get_img_dimension();
  std::vector<uint8_t>& get_pixel_data();

private:
  std::string file;
  uint32_t width;
  uint32_t height;
  std::vector<uint8_t> image; // Hold the image data.
  GrayScaleWeightStruct weight = {.R = 0.2126f, .G = 0.7152f, .B = 0.0722};

};

#endif // __PNG_IMG_H
