#ifndef __MEDIAN_HC_H
#define __MEDIAN_HC_H

#include "median_filter.h"
#include <cmath>
// #include <hcc/hc.hpp> // For HCC compiler
#include "hc.hpp" // For HCC compiler
#include <algorithm>
#include <iostream>

class MedianFilterHC : public MedianFilter {
  public:
    MedianFilterHC(size_t _width, size_t _height, \
      std::vector<uint8_t>& _image)\
      : width(_width), height(_height), image(_image){}

      std::vector<uint8_t> compute_median_filter(size_t radius);
      std::vector<uint8_t> calc_hist(size_t radius, std::vector<uint8_t>& pxl);
      std::vector<uint8_t> calc_fixed_3(std::vector<uint8_t>& pxl);
      std::vector<uint8_t> calc_reg_sort_3(std::vector<uint8_t>& pxl);
      std::vector<uint8_t> calc_bbl_sort_3(std::vector<uint8_t>& pxl);
      std::vector<uint8_t> calc_lds_3(std::vector<uint8_t>& pxl);
      // std::vector<uint8_t> calc_sort_2x_3(std::vector<uint8_t>& pxl);

      std::vector<uint8_t> echo_kernel();

  private:
    uint8_t local_median_sort(std::vector<uint8_t>& window, size_t radius);
    uint32_t index_2D(uint32_t x, uint32_t y);

    const size_t width;
    const size_t height;
    std::vector<uint8_t>& image;
};

#endif // __MEDIAN_HC_H
