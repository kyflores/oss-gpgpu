#ifndef __MEDIAN_FILTER_H
#define __MEDIAN_FILTER_H

#include <iostream>
#include <vector>
#include <array>
#include <chrono>
#include <cstdint>

class MedianFilter {
  public:
    void get_start_time();
    void get_end_time();
    double get_execution_time();
    //Double return is execution time.
    virtual std::vector<uint8_t> compute_median_filter(size_t R) = 0;

    double last_execution_time;
  protected:
    std::chrono::high_resolution_clock::time_point start_time;
    std::chrono::high_resolution_clock::time_point end_time;

};

#endif // __MEDIAN_FILTER_H
