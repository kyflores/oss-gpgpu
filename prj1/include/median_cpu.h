#ifndef __MEDIAN_CPU_H
#define __MEDIAN_CPU_H

#include "median_filter.h"
#include "png_img.h"
#include <algorithm>
#include <iostream>

class MedianFilterCPU : public MedianFilter {
  //Radius 1 median filter
  public:
    // Insert parameters manually
    MedianFilterCPU(size_t _width, size_t _height, \
      std::vector<uint8_t>& _image)\
      : width(_width), height(_height), image(_image){}

      std::vector<uint8_t> compute_median_filter(size_t radius);
      //impl_0 is sort based, and directly implements the defintion of a median.
      std::vector<uint8_t> calc_sort_3(std::vector<uint8_t>& pxl);

      //impl_1 scans a histogram, but is otherwise identical.
      std::vector<uint8_t> calc_hist(size_t radius,std::vector<uint8_t>& pxl);
      std::vector<uint8_t> calc_fixed_3(std::vector<uint8_t>& pxl);

  private:

    uint8_t local_median_sort(std::vector<uint8_t>& window, size_t radius);
    uint8_t local_median_hist(std::vector<uint8_t>& window, size_t radius);
    inline uint32_t index_2D(uint32_t x, uint32_t y);

    const size_t width;
    const size_t height;
    std::vector<uint8_t>& image;
};

#endif // __MEDIAN_CPU_H
