#include "png_img.h"

std::vector<std::uint8_t> PNGimg::decode_to_arr(){
  //decode
  std::vector<std::uint8_t> _image;
  std::uint32_t error = lodepng::decode(
    _image, this->width, this->height, (file+".png").data());

  //if there's an error, display it
  if(error)
    std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;

    // std::cout << "Reading file " << this->file << std::endl;
    // std::cout << "Read " << _image.size() << " bytes" << std::endl;
  return _image;
}

void PNGimg::encode_from_arr(std::vector<std::uint8_t>& data, std::string append){
  std::string save_name = file + append + ".png";
  std::uint32_t error = lodepng::encode(
    save_name.data(), data, this->width, this->height);

  //if there's an error, display it
  if(error)
    std::cout << "encoder error " << error << ": "<< lodepng_error_text(error) << std::endl;

  // std::cout << "Wrote " << save_name << std::endl;
  // std::cout << "Wrote " << data.size() << " bytes" << std::endl;
}

void PNGimg::encode(){
  encode_from_arr(this->image);
}

void PNGimg::decode(){
  this->image = decode_to_arr();
}

std::vector<std::uint8_t> PNGimg::get_grayscale(){
  std::vector<std::uint8_t> grayscale_out;
  if (!(this->image.size() % 4 == 0)){
    std::cout << "Byte size of image data not divisible by 4!" << std::endl;
    return grayscale_out;
  }
  std::uint32_t pxl_count = this->image.size() / 4;
  grayscale_out.resize(pxl_count);
  for(std::uint32_t i=0; i < this->image.size(); i+=4){
    grayscale_out[i/4] = weight.R*image[i] + weight.G * image[i+1] \
    + weight.B * image[i+2]; //ignore alpha
  }
  return grayscale_out;
}

std::vector<std::uint8_t> PNGimg::save_grayscale(std::vector<std::uint8_t>& gray_img){
  std::vector<std::uint8_t> rgba_out;
  rgba_out.resize(gray_img.size()*4); // Expand to RGBA
  for(int i = 0; i < rgba_out.size(); i +=4){
    std::uint8_t pxl_val = gray_img[i/4];
    // std::cout<<pxl_val<<std::endl;
    rgba_out[i] = pxl_val;
    rgba_out[i+1] = pxl_val;
    rgba_out[i+2] = pxl_val;
    rgba_out[i+3] = 0xFF;
  }
  return rgba_out;
}

PNGimg::ImgDimStruct PNGimg::get_img_dimension(){
  return {.width = this->width, .height = this->height};
}

std::vector<uint8_t>& PNGimg::get_pixel_data(){
  return this->image;
}
