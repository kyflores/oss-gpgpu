def bblsort(arr, length):
    for start in range(length-1):
        for i in range(length-start-1):
            if(arr[i]>arr[i+1]):
                (arr[i], arr[i+1]) = (arr[i+1], arr[i])
    return arr

arr = [1,5,3,7,15,67,2,8]
print(bblsort(arr, len(arr)))
