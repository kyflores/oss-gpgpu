#include "median_hc.h"

std::vector<uint8_t> MedianFilterHC::compute_median_filter(size_t radius){
  if (this->image.size() != (this->width * this->height)){
    std::cout << "Image array size does not match given dimensions." << std::endl;
  }
  return calc_fixed_3(this->image);
}

std::vector<uint8_t> MedianFilterHC::calc_hist(size_t radius, std::vector<uint8_t>& pxl){
  // Will use a histogram based median.

  int32_t R = (int32_t)radius;
  size_t n = (2*R+1)*(2*R+1);

  std::vector<uint8_t> output(this->height*this->width, 0);

  // Extent of the total image, used for array sizing.
  // NOTE TO SELF: when constructing extents, it's rows by cols (or height by width)
  // All indexing schemes are row, then col like when we talk about matrices.
  hc::extent<2> iext(this->height, this->width);

  // Copy source data array into GPU array.
  // hc::array_view<uint8_t,2> img_in(iext, image.data());
  const hc::array<uint8_t,2> img_in(iext, pxl.begin(), pxl.end());
  // Second copy of input to overwrite. Read from img_in, write to img_out.
  hc::array<uint8_t,2> img_out(iext, output.begin(), output.end());

  // Use the full image extent - edges will wrap around here.
  std::cout <<"Kernel Launch."<<std::endl;

  hc::parallel_for_each(iext, [=, &img_in, &img_out](hc::index<2> idx) [[hc]] {
    const int32_t x = idx[0]; // Gets the height dim (backwards I know!)
    const int32_t y = idx[1]; // Gets the width dim
    uint32_t bins[256] = {0};
    int32_t i,j;
    for(i = x-R; i <= x+R; i++){
      for(j = y-R; j <= y+R; j++){
        // t_idx dimensions might be flipped
        uint8_t val = img_in[i][j];
        bins[val] +=1;
      }
    }
    uint32_t acc = 0;
    int32_t z;
    for(z = 0; z < 256; z++){
      acc += bins[z];
      if(acc > (n/2)){
        break;
      }
    }
    img_out[x][y] = z;
  }); // end parallel_for_each

  hc::copy(img_out, output.begin());

  return output;
}

std::vector<uint8_t> MedianFilterHC::calc_fixed_3(std::vector<uint8_t>& pxl){
  // Will use a histogram based median.

  static constexpr size_t r = 1;
  static constexpr size_t n = (2*r+1)*(2*r+1);

  std::vector<uint8_t> output(this->height*this->width, 0);

  // Extent of the total image, used for array sizing.
  // NOTE TO SELF: when constructing extents, it's rows by cols (or height by width)
  // All indexing schemes are row, then col like when we talk about matrices.
  hc::extent<2> iext(this->height, this->width);

  // Copy source data array into GPU array.
  // hc::array_view<uint8_t,2> img_in(iext, image.data());
  const hc::array<uint8_t,2> img_in(iext, pxl.begin(), pxl.end());
  // Second copy of input to overwrite. Read from img_in, write to img_out.
  hc::array<uint8_t,2> img_out(iext, output.begin(), output.end());

  // Use the full image extent.

  hc::completion_future ft = hc::parallel_for_each(iext, [=, &img_in, &img_out](hc::index<2> idx) [[hc]] {
    const int row = idx[0];
    const int col = idx[1];
    uint32_t bins[256] = {0};


    bins[img_in[row-1][col-1]] +=1;
    bins[img_in[row-1][col]] +=1;
    bins[img_in[row-1][col+1]] +=1;

    bins[img_in[row][col-1]] +=1;
    bins[img_in[row][col]] +=1;
    bins[img_in[row][col+1]] +=1;

    bins[img_in[row+1][col-1]] +=1;
    bins[img_in[row+1][col]] +=1;
    bins[img_in[row+1][col+1]] +=1;

    uint32_t acc = 0;
    int32_t z;
    for(z = 0; z < 256; z++){
      acc += bins[z];
      if(acc > (n/2)){
        break;
      }
    }
    img_out[row][col] = z;
  }); // end parallel_for_each

  hc::copy(img_out, output.begin());

  uint64_t diff = ft.get_end_tick() - ft.get_begin_tick();
  last_execution_time = (double)diff / (double)ft.get_tick_frequency();
  while(!ft.is_ready()){}

  return output;
}


inline void s(uint32_t* a, uint32_t* b) [[hc]]{
  uint32_t tmp;
  if (*a > *b) { tmp = *b; *b = *a; *a = tmp;}
}

inline void bbl_sort_9(uint32_t *arr) [[hc]]{
  static constexpr uint32_t len = 9;
  #pragma unroll
  for(uint32_t start = 0; start < len-1; start++){
    for(uint32_t i = 0; i < len-start-1; i++){
      s(&arr[i], &arr[i+1]);
    }
  }
}

std::vector<uint8_t> MedianFilterHC::calc_bbl_sort_3(std::vector<uint8_t>& pxl){
  // Uses extrema elimination process to reduce total register count and
  // hopefully increase potential concurrency

  std::vector<uint8_t> output(this->height*this->width, 0);
  hc::extent<2> iext(this->height, this->width); //Row, col
  const hc::array<uint8_t,2> img_in(iext, pxl.begin(), pxl.end());
  hc::array<uint8_t,2> img_out(iext, output.begin(), output.end());


  hc::completion_future ft = hc::parallel_for_each(iext, [=, &img_in, &img_out](hc::index<2> idx) [[hc]] {
    const int row = idx[0];
    const int col = idx[1];
    // The idea here is to reduce register usage down.
    uint32_t a[9];

    a[0] = img_in[row-1][col-1];
    a[1] = img_in[row-1][col];
    a[2] = img_in[row-1][col+1];
    a[3] = img_in[row][col-1];
    a[4] = img_in[row][col];
    a[5] = img_in[row][col+1];
    a[6] = img_in[row+1][col-1];
    a[7] = img_in[row+1][col];
    a[8] = img_in[row+1][col+1];
    ::bbl_sort_9(a);


    img_out[row][col] = a[4];
  }); // end parallel_for_each

  hc::copy(img_out, output.begin());

  uint64_t diff = ft.get_end_tick() - ft.get_begin_tick();
  last_execution_time = (double)diff / (double)ft.get_tick_frequency();

  while(!ft.is_ready()){}
  return output;
}

// REFERENCE: Listing 4.3: P.42 from "Designing Scientific Aapplications on GPUs"
// These are the sorting networks written out for 3-6.
// Usually at top of file, but here for clarity.
#define min3(a, b, c) ::s(a,b); ::s(a,c);
#define max3(a, b, c) ::s(b,c); ::s(a,c);
#define minmax3(a, b, c) max3(a, b, c); ::s(a,b);
#define minmax4(a, b, c, d) ::s(a,b); ::s(c,d); ::s(a,c); ::s(b,d);
#define minmax5(a, b, c, d, e) ::s(a,b); ::s(c,d); min3(a, c, e); max3(b, d, e);
#define minmax6(a, b, c, d, e, f) ::s(a,d); ::s(b,e); ::s(c,f); min3(a, b, c); max3(d, e, f);


std::vector<uint8_t> MedianFilterHC::calc_reg_sort_3(std::vector<uint8_t>& pxl){
  // Uses extrema elimination process to reduce total register count and
  // hopefully increase potential concurrency

  std::vector<uint8_t> output(this->height*this->width, 0);
  hc::extent<2> iext(this->height, this->width); //Row, col
  const hc::array<uint8_t,2> img_in(iext, pxl.begin(), pxl.end());
  hc::array<uint8_t,2> img_out(iext, output.begin(), output.end());


  hc::completion_future ft = hc::parallel_for_each(iext, [=, &img_in, &img_out](hc::index<2> idx) [[hc]] {
    const int row = idx[0];
    const int col = idx[1];
    // The idea here is to reduce register usage down.
    uint32_t a0, a1, a2, a3, a4, a5; // Doesn't compile if uint8_t...?

    a0 = img_in[row-1][col-1];
    a1 = img_in[row-1][col];
    a2 = img_in[row-1][col+1];

    a3 = img_in[row][col-1];
    a4 = img_in[row][col];
    a5 = img_in[row][col+1];

    minmax6(&a0,&a1,&a2,&a3,&a4,&a5);
    a5 = img_in[row+1][col-1];
    minmax5(&a1,&a2,&a3,&a4,&a5);
    a5 = img_in[row+1][col];
    minmax4(&a2,&a3,&a4,&a5);
    a5 = img_in[row+1][col+1];
    minmax3(&a3, &a4, &a5);

    img_out[row][col] = a4;
  }); // end parallel_for_each

  hc::copy(img_out, output.begin());

  uint64_t diff = ft.get_end_tick() - ft.get_begin_tick();
  last_execution_time = (double)diff / (double)ft.get_tick_frequency();

  while(!ft.is_ready()){}
  return output;
}

inline void a_s(uint32_t* a, uint32_t* b) [[hc]]{
  uint32_t tmp;
  if (*a > *b) { tmp = *b; *b = *a; *a = tmp;}
}

std::vector<uint8_t> MedianFilterHC::calc_lds_3(std::vector<uint8_t>& pxl){

  static constexpr uint32_t t_rows = 32;
  static constexpr uint32_t t_cols = 256;

  std::vector<uint8_t> output(this->height*this->width, 0);
  hc::extent<2> iext(this->height, this->width);
  hc::tiled_extent<2> tiext = iext.tile(t_rows, t_cols);
  const hc::array<uint8_t,2> img_in(iext, pxl.begin(), pxl.end());
  hc::array<uint8_t,2> img_out(iext, output.begin(), output.end());

  hc::completion_future ft = hc::parallel_for_each(tiext, [=, &img_in, &img_out](hc::tiled_index<2> idx) [[hc]] {
    // The coordinates within the full grid
    const uint32_t g_row = idx.global[0];
    const uint32_t g_col = idx.global[1];

    // In the tiled_index
    const uint32_t l_row = idx.local[0];
    const uint32_t l_col = idx.local[1];

    // Need to be big enough to hold the non-center pixel
    // Branchless scheme that redundantly loads in parallel, but should get everything.
    // Only loads corners, which reduces some redundancy.
    // [+][0][+]
    // [0][0][0]
    // [+][0][+]
    tile_static uint8_t pxl_lds[t_rows+2][t_cols+2];

    pxl_lds[l_row][l_col] = img_in[g_row-1][g_col-1];
    // pxl_lds[l_row][l_col+1] = img_in[g_row-1][g_col];
    pxl_lds[l_row][l_col+2] = img_in[g_row-1][g_col+1];
    // pxl_lds[l_row+1][l_col] = img_in[g_row][g_col-1];
    // pxl_lds[l_row+1][l_col+1] = img_in[g_row][g_col];
    // pxl_lds[l_row+1][l_col+2] = img_in[g_row][g_col+1];
    pxl_lds[l_row+2][l_col] = img_in[g_row+1][g_col-1];
    // pxl_lds[l_row+2][l_col+1] = img_in[g_row+1][g_col];
    pxl_lds[l_row+2][l_col+2] = img_in[g_row+1][g_col+1];

    idx.barrier.wait_with_tile_static_memory_fence();


    uint32_t a0, a1, a2, a3, a4, a5;
    a0 = pxl_lds[l_row][l_col];
    a1 = pxl_lds[l_row][l_col+1];
    a2 = pxl_lds[l_row][l_col+2];
    a3 = pxl_lds[l_row+1][l_col];
    a4 = pxl_lds[l_row+1][l_col+1];
    a5 = pxl_lds[l_row+1][l_col+2];
    minmax6(&a0,&a1,&a2,&a3,&a4,&a5);
    a5 = pxl_lds[l_row+2][l_col];
    minmax5(&a1,&a2,&a3,&a4,&a5);
    a5 = pxl_lds[l_row+2][l_col+1];
    minmax4(&a2,&a3,&a4,&a5);
    a5 = pxl_lds[l_row+2][l_col+2];
    minmax3(&a3, &a4, &a5);

    img_out[g_row][g_col] = a4;
  }); // end parallel_for_each
  hc::copy(img_out, output.begin());

  uint64_t diff = ft.get_end_tick() - ft.get_begin_tick();
  last_execution_time = (double)diff / (double)ft.get_tick_frequency();

  while(!ft.is_ready()){}
  return output;
}

std::vector<uint8_t> MedianFilterHC::echo_kernel(){
  //Debug kernel

  std::vector<uint8_t> output(height*width, 0);

  hc::extent<2> iext(this->height, this->width);

  const hc::array<uint8_t,2> img_in(iext, image.begin(), image.end());
  hc::array<uint8_t,2> img_out(iext, output.begin(), output.end());

  std::cout <<"Kernel Launch."<<std::endl;
  hc::parallel_for_each(iext, [=, &img_in, &img_out](hc::index<2> idx) [[hc]] {
    const int row = idx[0];
    const int col = idx[1];
    img_out[row][col] = (col-row);
    // img_out[idx] = ( img_in[idx.global[0]] + img_in[idx.global[0]] + img_in[idx.global[0]] ) / 3;
  });

  hc::copy(img_out, output.begin());
  return output;
}
