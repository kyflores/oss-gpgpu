#include "median_cpu.h"

inline uint32_t MedianFilterCPU::index_2D(uint32_t x, uint32_t y){
  return x + this->width*y;
}

std::vector<uint8_t> MedianFilterCPU::compute_median_filter(size_t radius){
    if (this->image.size() != (this->width * this->height)){
      std::cout << "Image array size does not match given dimensions." << std::endl;
    }
    return calc_fixed_3(image);
}

inline void s(uint32_t* a, uint32_t* b){
  uint32_t tmp;
  if (*a > *b) { tmp = *b; *b = *a; *a = tmp;}
}
inline void bbl_sort_9(uint32_t *arr){
  static constexpr uint32_t len = 9;
  #pragma unroll
  for(uint32_t start = 0; start < len-1; start++){
    for(uint32_t i = 0; i < len-start-1; i++){
      s(&arr[i], &arr[i+1]);
    }
  }
}

std::vector<uint8_t> MedianFilterCPU::calc_sort_3(std::vector<uint8_t>& pxl){
  // Slow filter that reloads the window and uses std::sort.
  // Copy to create a vector to write to.
  std::vector<uint8_t> data = std::vector<uint8_t>(pxl);
  get_start_time();

  //Must be int because of negative indexes and wrap arounds.
  int32_t x,y;
  static constexpr int32_t r = 1;

  for(x = r; x < (width-r); x++){
    for(y = r; y < (height-r); y++){
      uint32_t tgt_idx = index_2D(x,y);
      uint32_t tmp[9];
      tmp[0] = pxl[index_2D(x-r, y-r)];
      tmp[1] = pxl[index_2D(x, y-r)];
      tmp[2] = pxl[index_2D(x+r, y-r)];
      tmp[3] = pxl[index_2D(x-r, y)];
      tmp[4] = pxl[index_2D(x, y)];
      tmp[5] = pxl[index_2D(x+r, y)];
      tmp[6] = pxl[index_2D(x-r, y+r)];
      tmp[7] = pxl[index_2D(x, y+r)];
      tmp[8] = pxl[index_2D(x+r, y+r)];
      bbl_sort_9(tmp);

      data[tgt_idx] = tmp[4];
    }
  }

  get_end_time();
  get_execution_time();
  return data;
}

std::vector<uint8_t> MedianFilterCPU::calc_fixed_3(std::vector<uint8_t>& pxl){
  //Identical but calls hist.
  std::vector<uint8_t> data(pxl);
  int32_t x,y;
  static constexpr size_t r = 1;
  static constexpr size_t n = (2*r+1)*(2*r+1);

  get_start_time();
  // For every pixel in x, y
  for(x = r; x < (width-r); x++){
    for(y = r; y < (height-r); y++){
      uint32_t tgt_idx = index_2D(x,y);
      uint8_t bins[256] = {0};
      bins[pxl[index_2D(x-r, y-r)]] += 1;
      bins[pxl[index_2D(x, y-r)]] += 1;
      bins[pxl[index_2D(x+r, y-r)]] += 1;

      bins[pxl[index_2D(x-r, y)]] += 1;
      bins[pxl[index_2D(x, y)]] += 1;
      bins[pxl[index_2D(x+r, y)]] += 1;

      bins[pxl[index_2D(x-r, y+r)]] += 1;
      bins[pxl[index_2D(x, y+r)]] += 1;
      bins[pxl[index_2D(x+r, y+r)]] += 1;

      int32_t z;
      uint32_t acc = 0;
      for(z = 0; z < 256; z++){
        acc += bins[z];
        if(acc > (n/2)){
          break;
        }
      }
      data[tgt_idx] = z;
    }
  }
  get_end_time();
  get_execution_time();
  return data;
}

std::vector<uint8_t> MedianFilterCPU::calc_hist(size_t radius, std::vector<uint8_t>& pxl){
  //Identical but calls hist.
  std::vector<uint8_t> data = std::vector<uint8_t>(pxl);
  int32_t x,y,i,j;
  int32_t R = (int32_t)radius;
  size_t n = (2*R+1)*(2*R+1);
  get_start_time();

  // For every pixel in x, y
  for(x = 0; x < width; x++){
    for(y = 0; y < height; y++){
      uint32_t tgt_idx = index_2D(x,y);
      uint32_t idx_counter = 0;
      uint8_t bins[256] = {0};
      // For every pixel under the window
      for(j = -R; j <= R; j++){
        for(i = -R; i <= R; i++){
          int32_t x_wrap = (x+i) % this->width;
          int32_t y_wrap = (y+j) % this->height;
          uint8_t val = pxl[index_2D(x_wrap, y_wrap)];
          bins[val] += 1;
        }
      }
      int32_t z;
      uint32_t acc = 0;
      for(z = 0; z < 256; z++){
        acc += bins[z];
        if(acc > (n/2)){
          break;
        }
      }
      data[tgt_idx] = z;
    }
  }
  get_end_time();
  get_execution_time();
  return data;
}

uint8_t MedianFilterCPU::local_median_sort(std::vector<uint8_t>& window, size_t R){
  // Sort Based Median.
  std::sort(window.begin(), window.end());
  size_t idx = ((2*R+1) * (2*R+1) - 1) / 2;
  uint8_t rval = window[idx];
  return rval;
}

// uint8_t MedianFilterCPU::local_median_hist(std::vector<uint8_t>& window, size_t R){
//   // Histogram based Median.
//   uint8_t bins[256] = {0}; // 256 bins derives from 8 bit color possibility.
//
//   // Bin filling step. Scan the window.
//   for(uint32_t i = 0; i < window.size(); i++){
//     bins[window[i]] += 1;
//   }
//
//   // Bin summing: Add bin heights until it reaches half the total value #.
//   uint32_t acc = 0;
//   uint8_t idx = 0;
//   while (acc < (window.size()/2)){
//     acc+=bins[idx];
//     idx++;
//   }
//   return idx;
// }
