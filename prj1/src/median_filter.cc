#include "median_filter.h"

void MedianFilter::get_start_time(){
  this->start_time = std::chrono::high_resolution_clock::now();
}

void MedianFilter::get_end_time(){
  this->end_time = std::chrono::high_resolution_clock::now();
}

double MedianFilter::get_execution_time(){
  if(this->end_time <= this->start_time){
    std::cout<<"Error: End time is earlier than start time."<<std::endl;
  }
  double result = std::chrono::duration_cast<std::chrono::microseconds>\
    (this->end_time - this->start_time).count();
  this->last_execution_time = result/(1000000); //output in seconds.
  return result;
}
