#include "png_img.h"
#include "median_cpu.h"
#include "median_hc.h"
#include "hcc/hc.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <cstdint>

void process_img(size_t r);
void echo();

int main(int argc, const char* argv[]){
  size_t r = 1;
  if (argc>1){
    r = atoi(argv[1]);
  }

  process_img(r);
  // echo();
}

void process_img(size_t r){

  std::string name = "../library.png";
  size_t width = 2080;
  size_t height = 1560;
  PNGimg img(name, width, height);

  img.decode();
  // img.encode();

  std::vector<uint8_t> gray = img.get_grayscale();
  std::vector<unsigned char> output(width*height, 0);

  MedianFilterCPU filterC(width, height, gray);
  MedianFilterHC filterH(width, height, gray);
  std::vector<uint8_t> resC = filterC.calc_fixed_3();
  double cpu = filterC.last_execution_time;

  std::vector<uint8_t> resH = filterH.calc_fixed_3();
  double gpu1 = filterH.last_execution_time;

  std::vector<uint8_t> rgba_gray = img.save_grayscale(gray);
  img.encode_from_arr(rgba_gray, "_gray");

  std::vector<uint8_t> rgba_grayC = img.save_grayscale(resC);
  img.encode_from_arr(rgba_grayC, "_cpu");

  std::vector<uint8_t> rgba_grayH = img.save_grayscale(resH);
  img.encode_from_arr(rgba_grayH, "_gpu1");

  resH = filterH.calc_reg_sort_3();
  double gpu2 = filterH.last_execution_time;

  rgba_grayH = img.save_grayscale(resH);
  img.encode_from_arr(rgba_grayH, "_gpu2");

  std::cout << "Self reported speedup of " << (cpu/gpu2) <<" times."<<std::endl;
  // std::vector<uint8_t> rgba_gray = img.save_grayscale(gray);

}

void echo(){
  // This function was useful for debugging how rows and cols are addressed.
  size_t width = 16;
  size_t height = 4;
  std::vector<uint8_t> inp(height*width,0);
  for(int idx = 0; idx < inp.size(); idx++){
    inp[idx] = idx;
  }
  std::cout<<"Input is "<<width<<" by "<<height<<std::endl;
  MedianFilterHC filter(width, height, inp);
  std::vector<uint8_t> res = filter.echo_kernel();

  for(int idx = 0; idx < inp.size(); idx++){
    std::cout<<(int)res[idx]<<", ";
    if((idx+1)%width==0){
      std::cout<<std::endl;
    }
  }
  std::cout<<std::endl;
}
