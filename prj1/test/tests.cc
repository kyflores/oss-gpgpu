#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#include "png_img.h"
#include "median_cpu.h"
#include "median_hc.h"
#include "hc.hpp"

#include <chrono>
#include <iostream>
#include <string>
#include <vector>
#include <cstdint>
#include <sys/stat.h>
#include <string>
#include <random>
#include <algorithm>
#include <iterator>
#include <functional>

// http://stackoverflow.com/questions/12774207/fastest-way-to-check-if-a-file-exist-using-standard-c-c11-c#12774387
inline bool exists (const std::string& name) {
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}

void warm_up(){
  uint32_t wdim = 2048;
  uint32_t hdim = 2048;

  std::vector<uint8_t> pxl(wdim*hdim, 8);
  std::vector<uint8_t> out(wdim*hdim, 0);

  hc::extent<2> ext(hdim,wdim); //rows, rows.
  hc::array<uint8_t,2> gpu_data(ext,pxl.begin(), pxl.end());
  hc::array<uint8_t,2> gpu_out(ext,out.begin(), out.end());

  hc::parallel_for_each(ext, [=, &gpu_data, &gpu_out] (hc::index<2> idx) [[hc]] {
    uint32_t tmp = gpu_data[idx[0]][idx[1]];
    gpu_out[idx[0]][idx[1]] = tmp*tmp;
  });
  hc::copy(gpu_out, out.begin());
  std::cout<<"Ran warm up kernel." <<std::endl;
}

TEST_CASE("Timed 'brighten' dummy kernel"){
  uint32_t wdim = 2000;
  uint32_t hdim = 2000;
  std::vector<uint8_t> pxl(wdim*hdim, 0);
  std::vector<uint8_t> out(wdim*hdim, 0);

  warm_up();

  static constexpr int iters = 50;
  double times = 0.0;
  for(int i = 0; i < iters; i++){
    hc::extent<2> ext(hdim,wdim);
    const hc::array<uint8_t,2> gpu_data(ext,pxl.begin(), pxl.end());
    hc::array<uint8_t,2> gpu_out(ext,out.begin(), out.end());

    hc::completion_future ft = hc::parallel_for_each(ext, [=, &gpu_data, &gpu_out] (hc::index<2> idx) [[hc]] {
      gpu_out[idx[0]][idx[1]] = gpu_data[idx[0]][idx[1]] + 100;
    });
    hc::copy(gpu_out, out.begin());

    uint64_t diff = ft.get_end_tick() - ft.get_begin_tick();
    times += (double)diff / (double)ft.get_tick_frequency();
  }

  std::cout << "Bright kernel took " << times/iters << " s on average."<<std::endl;
  for(int i = 0; i < out.size(); i++){
    CHECK(out[i] == 100);
  }
}

/*
TEST_CASE("Compute the median filter of a small array."){
  size_t width = 8;
  size_t height = 8;
  std::vector<uint8_t> inp(height*width,0);
  for(int idx = 0; idx < inp.size(); idx++){
    inp[idx] = idx;
  }
  std::cout<<"Input is "<<width<<" by "<<height<<std::endl;

  SUBCASE("Calculate on CPU"){
    std::cout << "Calculation on CPU.";
    MedianFilterCPU filter(width, height, inp);
    std::vector<uint8_t> res = filter.calc_fixed_3(inp);

    for(int idx = 0; idx < inp.size(); idx++){
      std::cout<<(int)res[idx]<<", ";
      if((idx+1)%width==0){
        std::cout<<std::endl;
      }
    }
    std::cout<<std::endl;
  }

  SUBCASE("Calculate on GPU with fixed 3 sort."){
    std::cout << "Calculation on GPU with fixed sort."<<std::endl;
    MedianFilterHC filter(width, height, inp);
    std::vector<uint8_t> res = filter.calc_reg_sort_3(inp);

    for(int idx = 0; idx < inp.size(); idx++){
      std::cout<<(int)res[idx]<<", ";
      if((idx+1)%width==0){
        std::cout<<std::endl;
      }
    }
    std::cout<<std::endl;
  }

  SUBCASE("Calculate on GPU with fixed 3 sort + LDS."){
    std::cout << "Calculation on GPU with LDS."<<std::endl;
    MedianFilterHC filter(width, height, inp);
    std::vector<uint8_t> res = filter.calc_lds_3(inp);

    for(int idx = 0; idx < inp.size(); idx++){
      std::cout<<(int)res[idx]<<", ";
      if((idx+1)%width==0){
        std::cout<<std::endl;
      }
    }
    std::cout<<std::endl;
  }
}
*/

/*
TEST_CASE("Calculate different sizes of noise images."){
  std::random_device rnd_device;
  // Specify the engine and distribution.
  std::mt19937 mersenne_engine(rnd_device());
  std::uniform_int_distribution<int> dist(0, 255);
  auto gen = std::bind(dist, mersenne_engine);
  static constexpr int iters = 5;
  static constexpr int sizes = 6;
  std::array<uint32_t, sizes> dims = {256, 512, 1024, 2048, 4096, 8192};
  std::array<std::vector<uint8_t>, sizes> test_data;
  std::array<std::vector<uint8_t>, sizes> reference;

  uint32_t count = 0;
  for(uint32_t i : dims){
    std::vector<uint8_t> vec(i*i);
    std::generate(begin(vec), end(vec), gen);
    test_data[count] = vec;
    count++;
  }

  // CPU Calculation. Not comparable to GPU computations because timing
  // method is different, and edges handled differently :(
  std::cout<<"Sweep Dim Size: CPU calc_fixed_3"<<std::endl;
  for(uint32_t i: dims){
    std::cout<<i<<", ";
  }
  std::cout<<std::endl;
  for(int idx = 0; idx < sizes; idx++){
    MedianFilterCPU filterC(dims[idx], dims[idx], test_data[idx]);
    double times = 0;
    filterC.calc_fixed_3(test_data[idx]);
    times += filterC.last_execution_time;
    std::cout<<times/iters<<", ";
  }
  std::cout<<std::endl;

  // Naive GPU implementation - Histogram counting w/ brute force parallelism
  // Also, generates the reference solution.
  std::cout<<"Sweep Dim Size: GPU calc_reg_sort_3"<<std::endl;
  for(uint32_t i: dims){
    std::cout<<i<<", ";
  }
  std::cout<<std::endl;
  for(int idx = 0; idx < sizes; idx++){
    MedianFilterHC filterH(dims[idx], dims[idx], test_data[idx]);
    double times = 0;
    for(uint32_t i = 0; i < iters; i++){
      reference[idx] = filterH.calc_reg_sort_3(test_data[idx]);
      times += filterH.last_execution_time;
    }
    std::cout<<times/iters<<", ";
  }
  std::cout<<std::endl;

  // Calculation with 6 pixel sorting network
  std::cout<<"Sweep Dim Size: GPU calc_reg_sort_3"<<std::endl;
  for(uint32_t i: dims){
    std::cout<<i<<", ";
  }
  std::cout<<std::endl;
  for(int idx = 0; idx < sizes; idx++){
    MedianFilterHC filterH(dims[idx], dims[idx], test_data[idx]);
    double times = 0;
    for(uint32_t i = 0; i < iters; i++){
      filterH.calc_fixed_3(test_data[idx]);
      times += filterH.last_execution_time;
    }
    std::cout<<times/iters<<", ";
  }
  std::cout<<std::endl;

  // Sorting network with a weak LDS tiling scheme.
  std::cout<<"Sweep Dim Size: GPU calc_lds_bad_1"<<std::endl;
  for(uint32_t i: dims){
    std::cout<<i<<", ";
  }
  std::cout<<std::endl;
  for(int idx = 0; idx < sizes; idx++){
    MedianFilterHC filterH(dims[idx], dims[idx], test_data[idx]);
    double times = 0;
    std::vector<uint8_t> res;
    for(uint32_t i = 0; i < iters; i++){
      res = filterH.calc_lds_bad_1(test_data[idx]);
      times += filterH.last_execution_time;
    }
    bool err = true;
    CHECK(res.size() == reference[idx].size());
    for(uint32_t i = 0; i < reference[idx].size(); i++){
      err &= (reference[idx][i] == res[i]);
    }
    CHECK(err);
    std::cout<<times/iters<<", ";
  }
  std::cout<<std::endl;

  // Sorting network with a better LDS tiling scheme.
  std::cout<<"Sweep Dim Size: GPU calc_lds_bad_2"<<std::endl;
  for(uint32_t i: dims){
    std::cout<<i<<", ";
  }
  std::cout<<std::endl;
  for(int idx = 0; idx < sizes; idx++){
    MedianFilterHC filterH(dims[idx], dims[idx], test_data[idx]);
    double times = 0;
    std::vector<uint8_t> res;
    for(uint32_t i = 0; i < iters; i++){
      res = filterH.calc_lds_bad_2(test_data[idx]);
      times += filterH.last_execution_time;
    }
    bool err = true;
    CHECK(res.size() == reference[idx].size());
    for(uint32_t i = 0; i < reference[idx].size(); i++){
      err &= (reference[idx][i] == res[i]);
    }
    CHECK(err);
    std::cout<<times/iters<<", ";
  }
  std::cout<<std::endl;

  // Sorting network with the best LDS scheme found.
  std::cout<<"Sweep Dim Size: GPU calc_lds_3"<<std::endl;
  for(uint32_t i: dims){
    std::cout<<i<<", ";
  }
  std::cout<<std::endl;
  for(int idx = 0; idx < sizes; idx++){
    MedianFilterHC filterH(dims[idx], dims[idx], test_data[idx]);
    double times = 0;
    std::vector<uint8_t> res;
    for(uint32_t i = 0; i < iters; i++){
      res = filterH.calc_lds_3(test_data[idx]);
      times += filterH.last_execution_time;
    }
    bool err = true;
    CHECK(res.size() == reference[idx].size());
    for(uint32_t i = 0; i < reference[idx].size(); i++){
      err &= (reference[idx][i] == res[i]);
    }
    CHECK(err);
    std::cout<<times/iters<<", ";
  }
  std::cout<<std::endl;
}
*/

TEST_CASE("Automatically make a noise image."){
  std::cout<< "Making a really big noise image w/ mersenne_engine"<<std::endl;
  //Generate a white noise image and save it.
  std::random_device rnd_device;
  // Specify the engine and distribution.
  std::mt19937 mersenne_engine(rnd_device());
  std::uniform_int_distribution<int> dist(0, 255);
  auto gen = std::bind(dist, mersenne_engine);
  static constexpr size_t sz = 2000;

  std::string name = "../noise";
  std::vector<uint8_t> vec(sz*sz);
  std::generate(begin(vec), end(vec), gen);
  PNGimg noise(name, sz, sz);
  std::vector<uint8_t> noise_img = noise.save_grayscale(vec);
  noise.encode_from_arr(noise_img, "");
}

TEST_CASE("Run an actual PNG image..") {
    //Verify in output.
    std::string name = "../noise";
    size_t width = 2000;
    size_t height = 2000;
    PNGimg img(name, width, height);
    img.decode();
    std::vector<uint8_t> dim = img.get_pixel_data();
    std::vector<uint8_t> gray = img.get_grayscale();
    REQUIRE(dim.size() == width*height*4);

    static constexpr uint32_t iters = 10;

    SUBCASE("Timed execution of Median Filter CPU by bubble sort"){
      MedianFilterCPU filterC(width, height, gray);

      double times = 0;
      for(uint32_t i = 0; i < iters; i++){
        filterC.calc_fixed_3(gray);
        times += filterC.last_execution_time;
      }
      gray = filterC.calc_sort_3(gray);
      std::cout<<"Timed execution of Median Filter CPU by sort"<<std::endl;
      std::cout<<"Average execution was "<<(times/iters)<<"s"<<std::endl;
      std::vector<uint8_t> rgba_grayC = img.save_grayscale(gray);
      img.encode_from_arr(rgba_grayC, "_cpuSORT");
      CHECK(exists(name+"_cpuSORT.png"));
    }
    SUBCASE("Timed execution of Median Filter CPU"){
      MedianFilterCPU filterC(width, height, gray);

      double times = 0;
      for(uint32_t i = 0; i < iters; i++){
        filterC.calc_fixed_3(gray);
        times += filterC.last_execution_time;
      }
      gray = filterC.calc_fixed_3(gray);
      std::cout<<"Timed execution of Median Filter CPU by hist"<<std::endl;
      std::cout<<"Average execution was "<<(times/iters)<<"s"<<std::endl;
      std::vector<uint8_t> rgba_grayC = img.save_grayscale(gray);
      img.encode_from_arr(rgba_grayC, "_cpuHIST");
      CHECK(exists(name+"_cpuHIST.png"));
    }
    SUBCASE("Timed execution of Median Filter GPU by histogram"){
      std::cout<<"Timed execution of Median Filter GPU by Histogram"<<std::endl;
      std::vector<uint8_t> gray = img.get_grayscale();
      warm_up();
      MedianFilterHC filterH(width, height, gray);
      double times = 0;
      for(uint32_t i = 0; i < iters; i++){
        filterH.calc_fixed_3(gray);
        times += filterH.last_execution_time;
      }
      gray = filterH.calc_fixed_3(gray);
      std::cout<<"Average execution was "<<(times/iters)<<"s"<<std::endl;
      std::vector<uint8_t> rgba_grayH = img.save_grayscale(gray);
      img.encode_from_arr(rgba_grayH, "_gpuHIST");
      CHECK(exists(name+"_gpuHIST.png"));
    }
    SUBCASE("Timed execution of Median Filter GPU by bubble sort"){
      std::cout<<"Timed execution of Median Filter GPU by bubble sort"<<std::endl;
      warm_up();
      MedianFilterHC filterH(width, height, gray);
      double times = 0;
      for(uint32_t i = 0; i < iters; i++){
        filterH.calc_bbl_sort_3(gray);
        times += filterH.last_execution_time;
      }
      gray = filterH.calc_reg_sort_3(gray);
      std::cout<<"Average execution was "<<(times/iters)<<"s"<<std::endl;
      std::vector<uint8_t> rgba_grayH = img.save_grayscale(gray);
      img.encode_from_arr(rgba_grayH, "_gpuBBLSORT");
      CHECK(exists(name+"_gpuBBLSORT.png"));
    }SUBCASE("Timed execution of Median Filter GPU by sort"){
      std::cout<<"Timed execution of Median Filter GPU by sort"<<std::endl;
      warm_up();
      MedianFilterHC filterH(width, height, gray);
      double times = 0;
      for(uint32_t i = 0; i < iters; i++){
        filterH.calc_reg_sort_3(gray);
        times += filterH.last_execution_time;
      }
      gray = filterH.calc_reg_sort_3(gray);
      std::cout<<"Average execution was "<<(times/iters)<<"s"<<std::endl;
      std::vector<uint8_t> rgba_grayH = img.save_grayscale(gray);
      img.encode_from_arr(rgba_grayH, "_gpuSORT");
      CHECK(exists(name+"_gpuSORT.png"));
    }
    SUBCASE("Timed execution of Median Filter GPU with LDS"){
      std::cout<<"Timed execution of Median Filter GPU with LDS."<<std::endl;
      warm_up();
      MedianFilterHC filterH(width, height, gray);
      double times = 0;
      for(uint32_t i = 0; i < iters; i++){
        filterH.calc_lds_3(gray);
        times += filterH.last_execution_time;
      }
      gray = filterH.calc_lds_3(gray);
      std::cout<<"Average execution was "<<(times/iters)<<"s"<<std::endl;
      std::vector<uint8_t> rgba_grayH = img.save_grayscale(gray);
      img.encode_from_arr(rgba_grayH, "_gpuLDS");
      CHECK(exists(name+"_gpuLDS.png"));
    }

}
