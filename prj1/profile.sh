/home/kyle/CodeXL/Output_x86_64/release/bin/CodeXLGpuProfiler --hsapmc -c ~/Storage/Repos/OSS-GPGPU/prj1/counters.txt --outputfile ./build/prf_prj1.csv ./build/prj1
compare ./noise_cpuSORT.png ./noise_cpuHIST.png -compose src diff_cpu.png
compare ./noise_cpuSORT.png ./noise_gpuSORT.png -compose src diff_cpu_hist.png
compare ./noise_gpuHIST.png ./noise_gpuBBLSORT.png -compose src diff_hist_bblsort.png
compare ./noise_gpuHIST.png ./noise_gpuSORT.png -compose src diff_hist_sort.png
compare ./noise_gpuHIST.png ./noise_gpuLDS.png -compose src diff_hist_lds.png
