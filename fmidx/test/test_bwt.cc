#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <string>
#include <iostream>
#include "doctest.h"
#include "bwt.h"

TEST_CASE("Check the behavior of ^ in string boolean compare"){
    std::string a = "^ABC";
    std::string b = "C^AB";
    CHECK((a>b));

    a = "^";
    b = "|";
    CHECK((a<b));
}

TEST_CASE("Test the slow BWT with vector table"){
    std::string test = "BANANA";
    std::string res = "";
    bwt_table(test, res);
    CHECK(res == "BNN^AA|A");
}