#include "bwt.h"

void bwt_table(std::string& s_in, std::string& s_out){
    std::string tmp = s_in;
    tmp.insert(0, "^");
    tmp +=  "|";
    std::cout << "Input String: " << tmp << std::endl;
    std::vector<std::string> rotations = std::vector<std::string>(tmp.length(), "");
    int i;
    // Copy all rotations into the vector.
    for(i = 0; i < tmp.length(); i++){
        rotations[i] = tmp;
        std::rotate(tmp.rbegin(), tmp.rbegin()+1, tmp.rend());
    }
    for(auto r : rotations){
        std::cout << r <<std::endl;
    }
    std::cout<<std::endl;

    // Assumption: a and b are same length, as one is a rotation of each other.
    // Also, they aren't the same and there's only one $.
    std::sort(rotations.begin(), rotations.end());

    s_out = "";
    for(auto c : rotations){
        std::cout << c << std::endl;
        s_out+= c.back();
    }

}