#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#include "mm.h"
#include <cmath>

TEST_CASE("Dense to Sparse conversion"){
  // Uses the example on the wiki page, "Sparse matrix"
  std::vector<float> A =
    {0,0,0,0,
     5,8,0,0,
     0,0,3,0,
     0,6,0,0};

  std::vector<float> sA;
  std::vector<size_t> iA;
  std::vector<size_t> jA;

  prj3::dense_to_csr(4, 4, A, sA, iA, jA);
  CHECK(sA[0] == 5.0f);
  CHECK(sA[1] == 8.0f);
  CHECK(sA[2] == 3.0f);
  CHECK(sA[3] == 6.0f);

  CHECK(iA[0] == 0);
  CHECK(iA[1] == 0);
  CHECK(iA[2] == 2);
  CHECK(iA[3] == 3);
  CHECK(iA[4] == 4);

  CHECK(jA[0] == 0);
  CHECK(jA[1] == 1);
  CHECK(jA[2] == 2);
  CHECK(jA[3] == 1);
}

TEST_CASE("Small CSRMV"){
  std::vector<float> A =
    {0,0,0,0,
     5,8,0,0,
     0,0,3,0,
     0,6,0,0};
  std::vector<float> sA;
  std::vector<size_t> iA;
  std::vector<size_t> jA;
  prj3::dense_to_csr(4, 4, A, sA, iA, jA);

  std::vector<float> V = {1,2,3,4};
  std::vector<float> V_res(4,0);

  SUBCASE("CPU Reference"){
    prj3::csrmv_ref(sA, iA, jA, V, V_res);
  }

  CHECK(V_res[0] == 0.0f);
  CHECK(V_res[1] == 21.0f);
  CHECK(V_res[2] == 9.0f);
  CHECK(V_res[3] == 12.0f);
}

TEST_CASE("Small CSRMM"){
  std::vector<float> A =
    {0,0,0,0,
     5,8,0,0,
     0,0,3,0,
     0,6,0,0};
  std::vector<float> sA;
  std::vector<size_t> iA;
  std::vector<size_t> jA;
  prj3::dense_to_csr(4, 4, A, sA, iA, jA);

  std::vector<float> dense =
    {1,1,1,1,
     1,1,1,1,
     1,1,1,1,
     1,1,1,1};

   std::vector<float> mRes(4 * 4, 0);

  SUBCASE("CPU Reference"){
    prj3::csrmm_ref(sA, iA, jA, 4, 4, dense, mRes);
  }

  CHECK(mRes[0] == 0);
  CHECK(mRes[4] == 13);
  CHECK(mRes[8] == 3);
  CHECK(mRes[12] == 6);
}
