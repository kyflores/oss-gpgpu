#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#include "mm.h"
#include <cmath>

/*
TEST_CASE("Very small Matrix verifiable by hand."){
  std::vector<float> A =
    {3,3,3,3,
     2,2,2,2,
     1,1,1,1};
  std::vector<float> B =
    {3,2,1,
     3,2,1,
     3,2,1,
     3,2,1};
  std::vector<float> C(9,0);
  size_t m = 3;
  size_t n = 4;
  size_t k = 3;

  SUBCASE("CPU Reference Fn"){
    prj3::mm_dense_ref(m, n, k, 1.0f, A, B, 1.0f, C);
  }

  SUBCASE("HC Function 0"){
    prj3::mm_dense_hip_0(m, n, k, 1.0f, A, B, 1.0f, C);
  }

  CHECK(std::abs(C[0] - 36.0f) < 0.0001f);
  CHECK(std::abs(C[4] - 16.0f) < 0.0001f);
  CHECK(std::abs(C[8] - 4.0f) < 0.0001f);
}

*/

TEST_CASE("Run large dense pseudorandom matrix"){
  const size_t m = 128;
  const size_t n = 512;
  const size_t k = 256;
  const float err_tol = 0.000001f;
  std::vector<float> A(m*n);
  std::vector<float> B(n*k);
  std::vector<float> C_ref(m*k,0);
  std::vector<float> C_test(m*k,0);
  prj3::rand_dense(m, n, k, A, B, 17);
  prj3::mm_dense_ref(m, n, k, 1.0f, A, B, 1.0f, C_ref);

  SUBCASE("Test HC 0 against ref"){
    prj3::mm_dense_hip_0(m, n, k, 1.0f, A, B, 1.0f, C_test);
    for(size_t i = 0; i < m*k; i++){
      CHECK(std::abs(C_ref[i]-C_test[i]) <  err_tol);
    }
  }

}
