// C := alpha*op( A )*op( B ) + beta*C
// LDA, LDB, LDC are not configurable.
#include <vector>
#include <iostream>
#include <cassert>
#include "hip/hip_runtime.h"

namespace prj3{

  void rand_dense(
    const size_t M, const size_t N, const size_t K,
    std::vector<float>& A, std::vector<float>& B, int seed
  );

  void dense_to_csr(
    const size_t R, const size_t C,
    const std::vector<float>& dA,
    std::vector<float>& sA, std::vector<size_t>& iA, std::vector<size_t>& jA
  );

  void mm_dense_ref(
    const size_t M, const size_t N, const size_t K,
    const float alpha, const std::vector<float>& A,
    std::vector<float>& B,
    const float beta, std::vector<float>& C
  );

  void mm_dense_hip_0(
    const size_t M, const size_t N, const size_t K,
    const float alpha, const std::vector<float>& A,
    std::vector<float>& B,
    const float beta, std::vector<float>& C
  );

  void csrmv_ref(
    std::vector<float>& sA, std::vector<size_t>& iA, std::vector<size_t>& jA,
    const std::vector<float>& dV, std::vector<float>& vRes
  );

  void csrmm_ref(
    std::vector<float>& sA, std::vector<size_t>& iA, std::vector<size_t>& jA,
    const size_t R, const size_t C, const std::vector<float>& dA, std::vector<float>& mRes
  );

  void csrmm_hip(

  );

}
