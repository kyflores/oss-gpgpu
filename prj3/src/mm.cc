// C := alpha*op( A )*op( B ) + beta*C
// LDA, LDB, LDC are not configurable.
// A is M x N -> A[r*N + c]
// B is N x K -> B[r*K + c]
// C is M x K -> C[r*K + c]

#include "mm.h"

/*
 * Generates a "bad" random matrix with a seed.
 */
void prj3::rand_dense(
  const size_t M, const size_t N, const size_t K,
  std::vector<float>& A, std::vector<float>& B, int seed
){
  std::srand(seed);
  for(auto &i : A){
    int num = std::rand();
    int den = std::rand();
    i = (float)num / (float)den;
  }

  for(auto &i : A){
    int num = std::rand();
    int den = std::rand();
    i = (float)num / (float)den;
  }
}

/*
 * Turns a dense matrix into a CSR formatted sparse matrix.
 * Probably really slow, but I'm just using this to make my testing easier.
 */
void prj3::dense_to_csr(
  const size_t R, const size_t C,
  const std::vector<float>& dA,
  std::vector<float>& sA, std::vector<size_t>& iA, std::vector<size_t>& jA
){
  std::vector<float> _sA;
  std::vector<size_t> _iA(1,0); // IA[0] = 0 by definition
  std::vector<size_t> _jA;
  for(size_t r = 0; r < R; r++){
    size_t iA_counter = 0;
    for(size_t c = 0; c < C; c++){
      if(dA[r*C+c] != 0){
        _sA.push_back(dA[r*C+c]);
        _jA.push_back(c);
        iA_counter++;
      }
    }
    _iA.push_back(iA_counter + _iA.back());
  }
  sA = _sA;
  iA = _iA;
  jA = _jA;
}

/*
 * Naive implementation of the matrix multiplication for the CPU
 */
void prj3::mm_dense_ref(
  const size_t M, const size_t N, const size_t K,
  const float alpha, const std::vector<float>& A,
  std::vector<float>& B,
  const float beta, std::vector<float>& C
){
  assert(A.size() == M * N);
  assert(B.size() == N * K);
  assert(C.size() == M * K);
  for(size_t r_A = 0; r_A < M; r_A++){ // For every row in A
    for(size_t c_B = 0; c_B < K; c_B++){ // For every column of B
      float acc = 0;
      for(size_t e = 0; e < N; e++){
        acc += alpha * A[r_A * N + e] * B[e * K + c_B];
      }
      C[r_A*K+c_B] = beta * C[r_A*K+c_B] + acc;
    }
  }
}

/*
 * Naive implementation of the matrix multiplication for the accelerator
 */

__global__ void dense_mm(hipLaunchParm lp,
    const float* __restrict__ dA,
    const float* __restrict__ dB,
    float*  __restrict__ dC,
    int M, int N, int K) {
    /* code */
    float acc = 0;
    int r = hipBlockIdx_y * hipBlockDim_y + hipThreadIdx_y;
    int c = hipBlockIdx_x * hipBlockDim_x + hipThreadIdx_x;
    for(int i = 0; i < N; i++){
        acc += dA[r*N + i] * dB[i*K+c];
    }
    dC[r*K+c] = acc;
}

void prj3::mm_dense_hip_0(
  const size_t M, const size_t N, const size_t K,
  const float alpha, const std::vector<float>& A,
  std::vector<float>& B,
  const float beta, std::vector<float>& C
){
  static constexpr int TPBX = 8;
  static constexpr int TPBY = 8;
  assert(A.size() == M * N);
  assert(B.size() == N * K);
  assert(C.size() == M * K);
  std::cout << "HC 0 called with (M,N,K) -> (" << M << "," << N << "," << K << ")" << std::endl;

  float* dA;
  float* dB;
  float* dC;
  hipMalloc((void**)&dA, M*N*sizeof(float));
  hipMalloc((void**)&dB, N*K*sizeof(float));
  hipMalloc((void**)&dC, M*K*sizeof(float));

  hipMemcpy(dA, A.data(), M * N * sizeof(float), hipMemcpyHostToDevice);
  hipMemcpy(dB, B.data(), N * K * sizeof(float), hipMemcpyHostToDevice);

  hipLaunchKernel(::dense_mm,
    dim3(K/TPBX,M/TPBY,1), //blocks
    dim3(TPBX,TPBY), //threads (per block)
    0, 0,
    dA, dB, dC, M, N, K);
  hipMemcpy(C.data(), dC, M * K * sizeof(float), hipMemcpyDeviceToHost);

}

/*
 * CSR matrix times dense vector
 */
void prj3::csrmv_ref(
  std::vector<float>& sA, std::vector<size_t>& iA,
  std::vector<size_t>& jA,
  const std::vector<float>& dV, std::vector<float>& vRes
){
  assert(dV.size() == vRes.size());
  assert(sA.size() == jA.size());

  for(size_t r = 1; r < iA.size(); r++){
    size_t si = iA[r-1];
    size_t ei = iA[r];
    float acc = 0;
    for(size_t idx = si; idx < ei; idx++){
      acc += sA[idx] * dV[jA[idx]];
    }
    vRes[r-1] = acc;
  }
}

/*
 * CSR matrix times dense matrix
 */
void prj3::csrmm_ref(
  std::vector<float>& sA, std::vector<size_t>& iA,
  std::vector<size_t>& jA,
  const size_t R, const size_t C, const std::vector<float>& dA, std::vector<float>& mRes
){
  assert(R * C == dA.size());
  assert(R * C == mRes.size());
  assert(sA.size() == jA.size());
  for(size_t cl = 0; cl < C; cl++){
    for(size_t r = 1; r < iA.size(); r++){
      size_t si = iA[r-1];
      size_t ei = iA[r];
      float acc = 0;
      for(size_t idx = si; idx < ei; idx++){
        acc += sA[idx] * dA[jA[idx] * C + cl];
      }
      mRes[(r-1)*C + cl] = acc;
    }
  }
}
